/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 50637
 Source Host           : localhost:3306
 Source Schema         : ahza

 Target Server Type    : MySQL
 Target Server Version : 50637
 File Encoding         : 65001

 Date: 29/07/2019 01:44:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id_kategori` int(15) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(25) DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `pelanggan`;
CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(25) NOT NULL COMMENT 'nama lengkap member',
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(15) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `alamat` text,
  `telepon` decimal(15,0) DEFAULT NULL,
  `provinsi` varchar(20) DEFAULT NULL,
  `kota` varchar(25) DEFAULT NULL,
  `kelurahan` varchar(25) DEFAULT NULL,
  `kecamatan` varchar(25) DEFAULT NULL,
  `kodepos` varchar(5) DEFAULT NULL,
  `pprovinsi` varchar(25) DEFAULT NULL,
  `pkota` varchar(25) DEFAULT NULL,
  `palamat` text,
  `pkelurahan` varchar(25) DEFAULT NULL,
  `pkecamatan` varchar(25) DEFAULT NULL,
  `pkodepos` varchar(5) DEFAULT NULL,
  `gambar` varchar(25) DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `tglgabung` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id_pelanggan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pengguna
-- ----------------------------
DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE `pengguna` (
  `id_pengguna` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(25) NOT NULL,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(15) DEFAULT NULL,
  `level` enum('0','1') DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id_pengguna`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pesanan
-- ----------------------------
DROP TABLE IF EXISTS `pesanan`;
CREATE TABLE `pesanan` (
  `id_pesanan` int(15) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) DEFAULT NULL,
  `id_produk` int(15) DEFAULT NULL,
  `jumlah` int(25) DEFAULT NULL,
  `deskripsipesanan` text,
  `status` enum('1','2','3','4') DEFAULT '1',
  `gambar` varchar(25) DEFAULT NULL,
  `gambar2` varchar(25) DEFAULT NULL,
  `gambar3` varchar(25) DEFAULT NULL,
  `buktipem` varchar(25) DEFAULT NULL,
  `tglpesan` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `metode_pengiriman` varchar(25) DEFAULT NULL,
  `totalbayar` decimal(15,0) DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pesanan`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `id_produk` int(15) NOT NULL AUTO_INCREMENT,
  `namaproduk` varchar(25) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `id_kategori` int(15) DEFAULT NULL,
  `gambar` varchar(50) DEFAULT NULL,
  `gambar2` varchar(50) DEFAULT NULL,
  `gambar3` varchar(50) DEFAULT NULL,
  `harga` decimal(15,2) DEFAULT NULL,
  `berat` int(15) DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_produk`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
