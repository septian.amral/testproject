/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : 127.0.0.1:3306
Source Database       : ahza

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-08-12 20:35:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for invoice
-- ----------------------------
DROP TABLE IF EXISTS `invoice`;
CREATE TABLE `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_no` varchar(25) DEFAULT NULL,
  `buktipem` varchar(25) DEFAULT NULL,
  `metode_pengiriman` varchar(25) DEFAULT NULL,
  `ongkir` decimal(15,0) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `last_update_date` datetime DEFAULT NULL,
  `last_update_by` varchar(25) DEFAULT NULL,
  `totalbayar` decimal(15,0) DEFAULT NULL,
  `jumlah` int(25) DEFAULT NULL,
  `id_pelanggan` int(11) DEFAULT NULL,
  `status` enum('1','2','3','4') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id_kategori` int(15) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(25) DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `pelanggan`;
CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(25) NOT NULL COMMENT 'nama lengkap member',
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(15) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `alamat` text,
  `telepon` decimal(15,0) DEFAULT NULL,
  `provinsi` varchar(20) DEFAULT NULL,
  `kota` varchar(25) DEFAULT NULL,
  `kelurahan` varchar(25) DEFAULT NULL,
  `kecamatan` varchar(25) DEFAULT NULL,
  `kodepos` varchar(5) DEFAULT NULL,
  `pprovinsi` varchar(25) DEFAULT NULL,
  `pkota` varchar(25) DEFAULT NULL,
  `palamat` text,
  `pkelurahan` varchar(25) DEFAULT NULL,
  `pkecamatan` varchar(25) DEFAULT NULL,
  `pkodepos` varchar(5) DEFAULT NULL,
  `gambar` varchar(25) DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `tglgabung` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id_pelanggan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pengguna
-- ----------------------------
DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE `pengguna` (
  `id_pengguna` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(25) NOT NULL,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(15) DEFAULT NULL,
  `level` enum('0','1') DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id_pengguna`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pesanan
-- ----------------------------
DROP TABLE IF EXISTS `pesanan`;
CREATE TABLE `pesanan` (
  `id_pesanan` int(15) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) DEFAULT NULL,
  `id_produk` int(15) DEFAULT NULL,
  `jumlah` int(25) DEFAULT NULL,
  `deskripsipesanan` text,
  `status` enum('1','2','3','4') DEFAULT '1',
  `gambar` varchar(25) DEFAULT NULL,
  `gambar2` varchar(25) DEFAULT NULL,
  `gambar3` varchar(25) DEFAULT NULL,
  `buktipem` varchar(25) DEFAULT NULL,
  `tglpesan` datetime DEFAULT NULL,
  `metode_pengiriman` varchar(25) DEFAULT NULL,
  `totalbayar` decimal(15,0) DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `invoice_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pesanan`) USING BTREE,
  KEY `fk_invoice_id` (`invoice_id`),
  CONSTRAINT `fk_invoice_id` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `id_produk` int(15) NOT NULL AUTO_INCREMENT,
  `namaproduk` varchar(25) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `id_kategori` int(15) DEFAULT NULL,
  `gambar` varchar(50) DEFAULT NULL,
  `gambar2` varchar(50) DEFAULT NULL,
  `gambar3` varchar(50) DEFAULT NULL,
  `harga` decimal(15,2) DEFAULT NULL,
  `berat` int(15) DEFAULT NULL,
  `lastupdate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_produk`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
