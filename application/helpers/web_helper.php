<?php
if (!defined('nama_sistem')) {
    define('nama_sistem', 'Sistem ...');
}

if (!defined('nama_toko')) {
    define('nama_toko', ' Ahza');
}

if (!defined('hak_cipta')) {
    define('hak_cipta', 'copyright &copy; Revika ' . date('Y'));
}

if (!defined('path_gambar')) {
    define('path_gambar', base_url('gambar/'));
}

if (!defined('api_ongkir')) {
    define('api_ongkir', 'http://api.rajaongkir.com/starter/');
}
if (!defined('key_ongkir')) {
    define('key_ongkir', 'https://api.rajaongkir.com/starter/province');
}

function cek_login()
{
    $CI=&get_instance();
    if (!$CI->session->has_userdata('logged_in') == true && !$CI->session->has_userdata('level') == 'admin') {
        redirect('admin/login', 'refresh');
    }
}

function cek_login_member()
{
    $CI=&get_instance();
    if (!$CI->session->has_userdata('level') == 'member') {
        redirect('member/daftar', 'refresh');
    }
}

function member_menu()
{
    $CI=&get_instance();
    if (!$CI->session->has_userdata('level') == 'member') {
        redirect('member/daftar', 'refresh');
    }
}