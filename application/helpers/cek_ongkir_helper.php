<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Client;

function panggil_guzzle($method, $url, $data = '')
{

    try {
        $client = new Client();

        $request = $client->request($method, api_ongkir . $url, [
            'connect_timeout' => 3.15,
            'headers'         => [
                'key'          => '1d3f1e8211b5fd1f69f4d3b8a17d88c1',
                'Content-Type' => 'application/xml',
            ],

        ]);
        $result = $request->getBody()->getContents();

    } catch (Exception $e) {
        log_message('error', $e->getMessage());
        $result = ['error' => 'API Error'];
    }
    return json_decode($result);
}


function post_data($method, $url, $olah)
{
    try {
        $client = new Client();

        $request = $client->request($method, api_ongkir . $url, [
            'headers'     => [
                'key' => '1d3f1e8211b5fd1f69f4d3b8a17d88c1',
            ],
            'form_params' => $olah,

        ]);
        $result = $request->getBody()->getContents();
    } catch (Exception $e) {
        log_message('error', $e->getMessage());
        $result = [
            'error' => 'API Error',
        ];
    }
    return json_decode($result, true);
}

function cek_provinsi($provinsi = '')
{
    $url   = 'province';
    $minta = panggil_guzzle('GET', $url);

    return $minta->rajaongkir->results;
}

function cek_kota($provinsi)
{
    $url   = 'city?province=' . $provinsi;
    $minta = panggil_guzzle('GET', $url);
    return $minta->rajaongkir->results;
}

function cek_harga($olah)
{
    $url   = 'cost';
    $minta = post_data('POST', $url, $olah);
    return $minta['rajaongkir']['results'][0]['costs'];
}

/* End of file cek_ongkir.php */
/* Location: ./application/helpers/cek_ongkir.php */
