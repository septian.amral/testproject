<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function tambah_member() {
        $data = [
            'username'   => $this->input->post('username'),
            'password'   => $this->input->post('password'),
            'nama'       => $this->input->post('nama_lengkap'),
            'telepon'    => $this->input->post('no_tlp'),
            'email'      => $this->input->post('email'),
            'gambar'     => $this->input->post('gambar'),
            'alamat'     => $this->input->post('alamat'),
            'provinsi'   => $this->input->post('provinsi'),
            'kota'       => $this->input->post('kota'),
            'kelurahan'  => $this->input->post('kelurahan'),
            'kecamatan'  => $this->input->post('kecamatan'),
            'kodepos'   => $this->input->post('kode_pos'),
            'palamat'     => $this->input->post('palamat'),
            'pprovinsi'   => $this->input->post('pprovinsi'),
            'pkota'       => $this->input->post('pkota'),
            'pkelurahan'  => $this->input->post('pkelurahan'),
            'pkecamatan'  => $this->input->post('pkecamatan'),
            'pkodepos'   => $this->input->post('pkode_pos'),
            'tglgabung'  => date('Y-m-d H:i:s'),
            'lastupdate' => date('Y-m-d H:i:s'),
        ];

        return $this->db->insert('pelanggan', $data);
    }

    public function data_member() {
        return $this->db->get('pelanggan')->result_array();
    }

    public function detail($id) {
        return $this->db->get_where('pelanggan', ['id_pelanggan' => $id])->result_array();
    }

    public function cek_masuk() {
        $data = [
            'username'   => $this->input->post('username'),
            'password'   => $this->input->post('password'),
        ];
         return $this->db->get_where('pelanggan', $data)->row();
    }

    public function hapus_member($id) {
        return $this->db->delete('pelanggan', ['id_pelanggan' => $id]);
    }

    public function ubah_member($id) {
        return $this->db->get_where('pelanggan', ['id_pelanggan' => $id])->result_array();
    }
}
