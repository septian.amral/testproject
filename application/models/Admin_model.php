<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function tambah_admin() {
        $data = [
            'nama'     => $this->input->post('nama'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'level'    => $this->input->post('level'),
        ];

        return $this->db->insert('pengguna', $data);
    }

    public function data_admin() {
        return $this->db->get('pengguna')->result_array();
    }

    public function hapus_admin($id, $status) {
         $data = [
            'status'     => ($status == 1 ? "1" :"0"),
        ];
        // $this->db->where('id_pengguna', $id)->update('pengguna', $data);
        return $this->db->where('id_pengguna', $id)->update('pengguna', $data);
    }

    public function ubah_admin($id) {
        return $this->db->get_where('pengguna', ['id_pengguna' => $id])->result_array();
    }

    public function simpan_ubah() {
        $data = [
            'nama'     => $this->input->post('nama'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'level'    => $this->input->post('level'),
        ];
        return $this->db->where('id_pengguna', $this->input->post('id_pengguna'))->update('pengguna', $data);
    }

    public function cek_user_pass() {
        $data = [
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
        ];

        return $this->db->get_where('pengguna', $data)->row();
    }
}
