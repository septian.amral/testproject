<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Invoice_model extends CI_Model
{

    private $table = 'invoice';

    public function __construct()
    {
        $this->load->database();
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $where)
    {
        $this->db->where($where);
        $this->db->update($this->table, $data);
    }

    public function get($where)
    {
        $this->db->select($this->table.'.*,pelanggan.username');
        $this->db->from($this->table);
        $this->db->join('pelanggan', $this->table.'.id_pelanggan = pelanggan.id_pelanggan');
        $this->db->order_by($this->table.'.status', 'ASC');
        $this->db->order_by($this->table.'.id', 'DESC');
        if ($where != null)
            $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    function _custom_query($mysql_query) {
        $query = $this->db->query($mysql_query);
        return $query;
    }
}

/* End of file Pesanan_model.php */
/* Location: ./application/models/Pesanan_model.php */
