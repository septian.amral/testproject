<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function tambah_kategori()
    {
        $data = [
            'kategori'   => $this->input->post('namakategori'),
            // 'deskripsi'  => $this->input->post('deskripsi'),
            'lastupdate' => date('Y-m-d H:i:s'),
        ];

        return $this->db->insert('kategori', $data);
    }

    public function data_kategori()
    {
        return $this->db->get('kategori')->result_array();
    }

    public function hapus_kategori($id)
    {
        return $this->db->delete('kategori', array('id_kategori' => $id));
    }
    public function ubah_kategori($id)
    {
        return $this->db->get_where('kategori', ['id_kategori' => $id])->result_array();
    }
    public function simpan_ubah()
    {
        $data = [
            'kategori'   => $this->input->post('namakategori'),
            // 'deskripsi'  => $this->input->post('deskripsi'),
            'lastupdate' => date('Y-m-d H:i:s'),
        ];
        return $this->db->where('id_kategori', $this->input->post('id_kategori'))->update('kategori', $data);
    }
}
