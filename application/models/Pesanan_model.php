<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pesanan_model extends CI_Model
{

    private $table = 'pesanan';

    public function __construct()
    {
        $this->load->database();
    }

    public function simpan_db($psn, $total, $date)
    {
        $data = [
            'id_pelanggan'      => $this->session->userdata('id_pelanggan'),
            'invoice_id' => $psn['invoice_id'],
            'id_produk'         => $psn['id'],
            'jumlah'            => $psn['qty'],
            'deskripsipesanan'  => $psn['options']['deskripsi'],
            'status'            => '1',
            'gambar'            => $psn['options']['gambar'],
            'gambar2'           => $psn['options']['gambar2'],
            'gambar3'           => $psn['options']['gambar3'],
            'tglpesan'          => $date,
            'metode_pengiriman' => $psn['metode'],
            //'totalbayar'        => $psn['total'],
            'totalbayar'        => $total,
            'lastupdate' => $date
        ];

        return $this->db->insert($this->table, $data);
    }

    public function update_db($id_pelanggan, $id_produk, $data)
    {
        $where = array('id_pelanggan' => $id_pelanggan, 'id_produk' => $id_produk);
        $this->db->where($where);
        $this->db->update($this->table, $data);
    }

    public function update($data, $where)
    {
        $this->db->where($where);
        $this->db->update($this->table, $data);
    }

    public function get($where)
    {
        /*
        $where = array('status' => 1);
        return $this->db->get_where($this->table, $where)->result_array();
        */
        //if ($where == '') $where = array('pesanan.status' => 1);
        $this->db->select('pesanan.*, pelanggan.username, produk.namaproduk, produk.harga');
        $this->db->from('pesanan');
        $this->db->join('pelanggan', 'pesanan.id_pelanggan = pelanggan.id_pelanggan');
        $this->db->join('produk', 'pesanan.id_produk = produk.id_produk');
        $this->db->order_by('id_pesanan', 'DESC');
        $this->db->order_by('status', 'ASC');
        if ($where != null)
            $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_current_page_records($limit, $start)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get($this->table);
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;
    }

    public function get_total() 
    {
        return $this->db->count_all($this->table);
    }
}

/* End of file Pesanan_model.php */
/* Location: ./application/models/Pesanan_model.php */
