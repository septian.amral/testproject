<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    private $table = "produk";

    public function tambah_produk($gbrprod)
    {

        $data = [
            'namaproduk'  => $this->input->post('namaproduk'),
            'deskripsi'   => $this->input->post('deskripsi'),
            'id_kategori' => $this->input->post('id_kategori'),
            'harga'       => $this->input->post('harga'),
            'berat'       => $this->input->post('berat'),
            'gambar'      => $gbrprod['gambar'],
            'gambar2'     => $gbrprod['gambar1'],
            'gambar3'     => $gbrprod['gambar2'],
            'lastupdate'  => date('Y-m-d H:i:s'),
        ];

        return $this->db->insert('produk', $data);
    }

    public function data_produk($id_kategori)
    {
        $this->db->SELECT('produk.namaproduk,produk.deskripsi, produk.gambar, produk.gambar2, kategori.kategori, produk.harga, produk.id_produk');
        $this->db->FROM('produk');
        $this->db->JOIN('kategori', 'kategori.id_kategori  = produk.id_kategori');
        (!empty($id_kategori)? $this->db->where('kategori.id_kategori', $id_kategori) : '');
        return $this->db->get()->result_array();
    }

    public function get_by_id($id_produk)
    {
        $this->db->SELECT('*');
        $this->db->FROM($this->table);
        $this->db->where('id_produk', $id_produk);
        return $this->db->get()->result_array()[0];
    }

    public function hapus_produk($id_produk)
    {
        return $this->db->delete('produk', ['id_produk' => $id_produk]);
    }

    public function ubah_produk($id_produk)
    {
        return $this->db->get_where('produk', ['id_produk' => $id_produk])->result_array();
    }

    public function cek_produk($id_produk)
    {
        $this->db->SELECT('produk.namaproduk, produk.gambar, produk.gambar2, produk.gambar3, kategori.kategori, produk.harga, produk.id_produk, produk.deskripsi');
        $this->db->FROM('produk');
        $this->db->JOIN('kategori', 'kategori.id_kategori  = produk.id_kategori');
        $this->db->where('produk.id_produk', $id_produk);
        return $this->db->get()->result_array();
    }

    public function simpan_ubah($gbrprod)
    {
        $data = [
            'namaproduk'  => $this->input->post('namaproduk'),
            'deskripsi'   => $this->input->post('deskripsi'),
            'id_kategori' => $this->input->post('id_kategori'),
            'harga'       => $this->input->post('harga'),
            'berat'       => $this->input->post('berat'),
            'gambar'      => $gbrprod['gambar'],
            'gambar2'     => $gbrprod['gambar1'].'2',
            'gambar3'     => $gbrprod['gambar2'].'3',
            'lastupdate'  => date('Y-m-d H:i:s'),
        ];

        return $this->db->where('id_produk', $this->input->post('id_produk'))->update('produk', $data);
    }
}
