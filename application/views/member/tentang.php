<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="<?=base_url()?>">Beranda</a>
                </li>
                <li class="active">Tentang Ahza</li>
            </ul>
        </div>
    </div>
</div>
<div class="welcome-area pt-70 pb-95">
    <div class="container">
        <!-- <div class="welcome-content text-center">
            <h5>Who Are We</h5>
            <h1>Welcome To Flone</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labor et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo consequat irure </p>
        </div> -->
        <div class="row">
        	<div class="col-lg-12 col-md-12">
        		<div class="welcome-content text-center mt-50">
        			<h5>Who Are We</h5>
		            <h1>Mengapa Harus Memilih Kami</h1>
		        </div>
        	</div>
	        <div class="col-xl-4 col-lg-4 col-md-4 col-4 col-sm-12">
	        	<div class="singleService">
	                <div class="serImg">
	                    <img src="<?= base_url() ?>images/desain.png" alt="">
	                </div>
	                <h6 class="lead text-uppercase bold">Kualitas Desain Terbaik</h6>
	                <p>Kami memberikan desain berkualitas dengan fasilitas produksi yang lengkap dalam satu area workshop</p>
	                <!-- <a href="#" class="learn">Learn more</a> -->
	            </div>
	        </div>
	        <div class="col-xl-4 col-lg-4 col-md-4 col-4 col-sm-12">
	        	<div class="singleService">
	                <div class="serImg">
	                    <img src="<?= base_url() ?>images/waktu.png" alt="">
	                </div>
	                <h6 class="lead text-uppercase bold">Hemat Waktu</h6>
	                <p>Ahza adalah online platform di mana Anda hanya memerlukan akses internet dari rumah untuk memperoleh desain tanpa bertemu Desainer</p>
	                <!-- <a href="#" class="learn">Learn more</a> -->
	            </div>
	        </div>
	        <div class="col-xl-4 col-lg-4 col-md-4 col-4 col-sm-12">
	        	<div class="singleService">
		        	<div class="serImg">
		                <img src="<?= base_url() ?>images/2.png" alt="">
		            </div>
		            <h6 class="lead text-uppercase bold">Harga Terjangkau</h6>
		            <p>
		            	Kami memberikan Harga yang Terjangkau dengan Kualitas Terbaik
		            </p>
		        </div>
	        </div>
	    </div>
    </div>
</div>