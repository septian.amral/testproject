<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="">Beranda</a>
                </li>
                <li class="active">Daftar</li>
            </ul>
        </div>
    </div>
</div>
<div class="login-register-area pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 ml-auto mr-auto">
                <div class="login-register-wrapper">
                    <div class="tab-content">
                        <div id="lg1" class="tab-pane active">
                            <div class="login-form-container">
                                <?=validation_errors();?>
                                <?php if ($this->session->flashdata('pesan')) { ?>
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        <strong>Perhatian!</strong> <?=$this->session->flashdata('pesan');?>.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php }?>
                                <div class="login-register-form">
                                    <?= form_open('member/buat_member'); ?>
                                    <legend>Data Akun</legend>  
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Username</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" name="username" placeholder="Username" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Password</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" type="password" name="password" placeholder="Password">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <legend>Data Pribadi </legend> 
                                        <div class="form-group">
                                            <label for="" class="col-xl-12 col-md-12 form-control-label">Nama Lengkap</label>
                                            <div class="col-xl-12 col-md-12">
                                                <input class="form-control" name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap" type="text">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Nomor Telepon </label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" name="no_tlp" id="no_tlp" placeholder="Nomor Telepon/ HP" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Email</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" name="email" placeholder="Email" type="email">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-xl-12 col-md-12 form-control-label">Foto Profil</label>
                                            <div class="col-xl-12 col-md-12">
                                                <input class="form-control" name="gambar" id="gambar" placeholder="Foto Profil" type="file">
                                            </div>
                                        </div>
                                    <legend>Alamat Rumah</legend>  
                                        <div class="form-group">
                                            <label for="" class="col-xl-12 col-md-12 form-control-label">Alamat Rumah</label>
                                            <div class="col-xl-12 col-md-12">
                                                <textarea class="form-control" rows="3" name="alamat" id="alamat"></textarea> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Provinsi</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <!-- <input class="form-control" name="provinsi" id="provinsi" placeholder="Provinsi" type="text"> -->
                                                        <select class="email s-email s-wid" name="provinsi" id="provinsi" onchange="return cari_kota()">
                                                            <?php foreach (cek_provinsi() as $key): ?>
                                                                <option value="<?php echo $key->province_id; ?>"><?php echo $key->province; ?></option>
                                                            <?php endforeach;?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Kota</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <!-- <input class="form-control" name="kota" id="kota" placeholder="Kota" type="text"> -->
                                                        <select class="email s-email s-wid" name="kota" id="kota" onselect="<?=base_url('keranjang/get_kota');?>">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Kelurahan</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" name="kelurahan" id="kelurhan" placeholder="Kelurahan" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Kecamatan</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" name="kecamatan" id="kecamatan" placeholder="Kecamatan" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Kode Pos</label>
                                                    <div class="col-xl-12 col-md-12" id="kodepos">
                                                        <input class='form-control' name='kode_pos' id='kode_pos' placeholder='Kode Pos' type='text'>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <legend>Alamat Pengiriman</legend>  
                                        <div class="form-group">
                                            <label for="" class="col-xl-12 col-md-12 form-control-label">Alamat Pengiriman</label>
                                            <div class="col-xl-12 col-md-12">
                                                <textarea class="form-control" rows="3" name="palamat" id="palamat"></textarea> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Provinsi</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" name="pprovinsi" id="pprovinsi" placeholder="Provinsi" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Kota</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" name="pkota" id="pkota" placeholder="Kota" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Kelurahan</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" name="pkelurahan" id="pkelurhan" placeholder="Kelurahan" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Kecamatan</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" name="pkecamatan" id="pkecamatan" placeholder="Kecamatan" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="col-xl-12 col-md-12 form-control-label">Kode Pos</label>
                                                    <div class="col-xl-12 col-md-12">
                                                        <input class="form-control" name="pkode_pos" id="pkode_pos" placeholder="Kode Pos" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <div class="pull-right">
                                        <button type="reset" class="btn btn-danger"> Batal</button>
                                        <button type="submit" class="btn btn-primary"> Simpan</button>
                                    </div>
                                    <?= form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function cari_kota() {
        // alert($('#provinsi').val());
        $.ajax({
            url:"<?=base_url('pengiriman/kota');?>",
            type: 'post',
            data:{
                provinsi: $('#provinsi').val(),
            },
            success: function( data, textStatus, jQxhr ){
                var val = JSON.parse(data);
                var len = val.length;
                $("#kota").empty();
                for(var i = 0; i<len; i++){
                    var id = val[i]['city_id'];
                    var name = val[i]['city_name'];
                    var kodepos = val[i]['postal_code'];
                    console.log(val[i]);
                    $("#kota").append("<option value='"+id+"'>"+name+"</option>");
                    // $("#kodepos").append("<input class='form-control' name='kode_pos' id='kode_pos' placeholder='Kode Pos' type='text' value='"+kodepos+"''>");
                }
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });
        return false;
    };
</script>