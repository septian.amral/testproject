<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li class="active">Cart Page </li>
            </ul>
        </div>
    </div>
</div>
<?php /*echo form_open('pesanan/simpan_pesanan');*/ ?>
<div class="cart-main-area pt-90 pb-100">
    <div class="container">
        <h3 class="cart-page-title">Your cart items</h3>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="table-content table-responsive cart-table-content">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Product Name</th>
                                <th>Until Price</th>
                                <th>Qty</th>
                                <th>Subtotal</th>
                                <th>action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1;?>
                            <?php if ($this->cart->contents()) {?>

                                <?php foreach ($this->cart->contents() as $items): ?>

                                    <?php echo form_hidden('rowid', $items['rowid']); ?>

                                    <tr>
                                     <td class="product-thumbnail">
                                        <a href="#"><img style="height:82px; width: 82px;" src="<?=base_url('gambar/').$items['gambar']?>" alt=""></a>
                                    </td>
                                    <td class="product-name">
                                        <?php echo $items['name']; ?>

                                        <!-- <?php if ($this->cart->has_options($items['rowid']) == true): ?>

                                            <p>
                                                <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>

                                                    <strong><?php echo $option_name; ?>:</strong><?php echo $option_value; ?><br />

                                                <?php endforeach;?>
                                            </p>

                                        <?php endif;?>
 -->
                                    </td>
                                    <td class="product-price-cart"><span class="amount">Rp. <?php echo $this->cart->format_number($items['price']); ?></span></td>
                                    <td><?php echo form_input(
                                        [
                                            'name' => 'qty', 
                                            'value' => $items['qty'], 
                                            'maxlength' => '3', 
                                            'size' => '5', 
                                            'class'=>"product-quantity",
                                            'readonly'=>"readonly"
                                            ]); ?></td>

                                            <td class="product-subtotal">Rp. <?php echo $this->cart->format_number($items['subtotal']); ?></td>
                                            <td class="product-remove">
                                                <a href="#"><i class="fa fa-pencil"></i></a>
                                                <a href="<?=base_url('keranjang/hapus_keranjang/' . $items['rowid']);?>"><i class="fa fa-times"></i></a>
                                            </td>
                                        </tr>

                                        <?php $i++;?>

                                    <?php endforeach;?>

                                    <tr>
                                        <td colspan="2"> </td>
                                        <td class="right"><strong>Total</strong></td>
                                        <td class="right">Rp.<?php echo $this->cart->format_number($this->cart->total()); ?></td>
                                    </tr>

                                </table>
                                <?php
                            } else {
                                echo "<td colspan='6'>Keranjang Anda Masing Kosong</td>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cart-shiping-update-wrapper">
                            <div class="cart-shiping-update">
                                <!-- <a href="#">Continue Shopping</a> -->
                            </div>
                            <div class="cart-clear">
                                <!-- <button type="submit">Update Shopping Cart</button> -->
                                <a href="<?=base_url('keranjang/hapus_keranjang/all')?>">Bersihkan Keranjang Belanja</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--
            </form>
            -->
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="cart-tax">
                        <div class="title-wrap">
                            <h4 class="cart-bottom-title section-bg-gray">Estimate Shipping And Tax</h4>
                        </div>
                        <div class="tax-wrapper">
                            <p>Enter your destination to get a shipping estimate.</p>
                            <div class="tax-select-wrapper">
                                <div class="tax-select">
                                    <label>
                                        * Provinsi
                                    </label>
                                    <select class="email s-email s-wid" name="provinsi" id="provinsi" onchange="return cari_kota()">
                                    <?php 
                                    ?> 
                                    
                                    <?php foreach (cek_provinsi() as $key): ?>
                                            <option value="<?php echo $key->province_id; ?>"><?php echo $key->province; ?></option>
                                        <?php endforeach;?>
                                    </select>

                                </div>
                                <div class="tax-select">
                                    <label>
                                        * Kota
                                    </label>
                                    <select class="email s-email s-wid" name="kota" id="kota" onselect="<?=base_url('keranjang/get_kota');?>">

                                    </select>
                                </div>
                                <div class="tax-select">
                                    <label>
                                        Berat
                                    </label>
                                    <?php $berat = 0; foreach ($this->cart->contents() as $items): ?>
                                        <?php $berat += ($items['qty'] * $items['berat']) / 1000; ?>
                                    <?php endforeach; ?>
                                    <input type="text" name="berat" id="berat" value="<?=$berat;?>" readonly>
                                </div>
                                <button class="cart-btn-2" type="button" onclick="return hitung_biaya()">Hitung Biaya</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">

                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="grand-totall">
                        <div class="title-wrap">
                            <h4 class="cart-bottom-title section-bg-gary-cart">Cart Total</h4>
                        </div>
                        <h5>Total products <span>Rp.<?php echo $this->cart->format_number($this->cart->total()); ?></span></h5>
                        <div id="total-shipping" class="total-shipping">
                            <h5>Pilih Metode Pengiriman</h5>
                            <ul>
                                
                            </ul>
                        </div>
                        <h4 class="grand-totall-title" id="grand-total"></h4>
                        <a data-toggle="modal" data-target="#peringtan" onclick="bind_data()" href="">Proceed to Checkout</a>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>
</div>
<!--
</form>
-->

<!-- The Modal -->
<div class="modal" id="peringtan">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open('pesanan/simpan_pesanan'); ?>
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Pemberitahuan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <input type="hidden" id="metode_pengiriman" name="metode_pengiriman" value="" />
                <input type="hidden" id="grand_total" name="grand_total" value="" />
                Setelah melakukan transaksi mohon untuk langsung membayar pesanan Anda.<br>
                * Oke untuk melanjutkan pembayaran<br>
                ** Batal untuk membatalkan transaksi<br>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <a onclick="peringtan(this)" type="submit" class="btn btn-success" data-dismiss="modal">
                            Oke
                </a>
                <a type="button" class="btn btn-danger" data-dismiss="modal">Batal</a>
            </div>
            </form>

        </div>
    </div>
</div>

<script type="text/javascript">
    function cari_kota() {
        // alert($('#provinsi').val());
        $.ajax({
            url:"<?=base_url('pengiriman/kota');?>",
            type: 'post',
            data:{
                provinsi: $('#provinsi').val(),
            },
            success: function( data, textStatus, jQxhr ){
                var val = JSON.parse(data);
                var len = val.length;
                $("#kota").empty();
                for(var i = 0; i<len; i++){
                    var id = val[i]['city_id'];
                    var name = val[i]['city_name'];
                    // console.log(val[i]);
                    $("#kota").append("<option value='"+id+"'>"+name+"</option>");
                }
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });
        return false;
    };

    function hitung_biaya() {
        var berat = $('#berat').val();
        var kota    = $('#kota').val();

        if (kota == null) { alert('Pilih Kota Terlebih Dahulu');}
        $.ajax({
            url:"<?=base_url('pengiriman/biaya');?>",
            type: 'post',
            data:{
                berat: berat,
                kota: kota,
            },
            success: function( data, textStatus, jQxhr ){
                console.log(data);
                var val = JSON.parse(data);
                var len = val.total.length;
                var no = 1;
                $("#total-shipping  ul").empty();
                for(var i = 0; i<len; i++){
                    var service = val.total[i]['service'];
                    var desc = val.total[i]['description'];
                    var price =val.total[i]['cost'][0].value;
                    var etd =val.total[i]['cost'][0].etd;
                    $("#total-shipping  ul").append('<li><input name="pilihpem" class="form-check-input" type="radio" value="'+price+'" onclick="jumlah_total('+price+')"><input id="metodepem" name="metodepem" type="hidden" value='+no+'><label>'+service+'</label><span> IDR '+price+'</span></li>');
                    no++;
                }
                $("#total-shipping  ul").append('<li><input name="pilihpem" class="form-check-input" type="radio" value="0" onclick="jumlah_total('+0+')"><input id="metodepem" name="metodepem" type="hidden" value="0"><label>Store Pickup </label><span> Ambil ditoko tanpa biaya</span></li>');
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });
        return false;
    }

    function jumlah_total(price) {
        var hasil = price+=<?=$this->cart->total()?>;
        // alert(hasil);
        $("#grand-total").html('Grand Total ' + '<span>'+hasil+'</span>' );
    }
    function peringtan(elem) {
        var metodepem = $('#metodepem').val();
        var biayapem = $("input[name='pilihpem']:checked").val();//$('#pilihpem').val();
        var grandtotal = $('#grand-total').find('span').text().trim();

        if (biayapem == null) {
            alert('Mohon pilih metode pembayaran anda.');
            //return false;
        }else{
            // alert(metodepem);
            //window.location.href = "<?= base_url() ?>pesanan/simpan_pesanan/"+metodepem+"/"+grandtotal;
            $(elem).closest('form').submit()
        }
    }

    function bind_data()
    {
        var metode_pengiriman = $("input[name='pilihpem']:checked").parent().find('label').first().text();
        var grand_total = $('#grand-total').find('span').text().trim();

        $('#metode_pengiriman').val(metode_pengiriman);
        $('#grand_total').val(grand_total);
    }
</script>