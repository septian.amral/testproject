<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="<?=base_url()?>">Beranda</a>
                </li>
                <li class="active"><?= $aktif ?> </li>
            </ul>
        </div>
    </div>
</div>
<div class="shop-area pt-95 pb-100">
    <div class="container">
        <div class="row flex-row-reverse">
            <div class="col-lg-9">
                <div class="shop-top-bar">
                    <div class="select-shoing-wrap">
                        <p>Showing 1–9 of 20 result</p>
                    </div>
                </div>
                <div class="shop-bottom-area mt-35">
                    <div class="tab-content jump">
                        <div id="shop-1" class="tab-pane active product-area" style="background-color: transparent;">
                            <div class="row">
                                <?php
                    foreach ($data_produk as $v_p):
                        ?>
                        <div class="col-xl-3 col-md-6 col-lg-4 col-sm-6">
                            <div class="product-wrap mb-25 scroll-zoom">
                                <div class="product-img">
                                    <a href="<?=base_url('produk/detail_produk/' . $v_p['id_produk']);?>">
                                        <img class="default-img" height="250" width="250" src="<?=base_url('gambar/'.$v_p['gambar']);?>" alt="">
                                        <img class="hover-img" class="default-img" height="250" width="250" src="<?=base_url('gambar/'.$v_p['gambar2']);?>" alt="">
                                    </a>
                                    <span class="purple"><?=$v_p['kategori']?></span>
                                    <div class="product-action">
                                        <div class="pro-same-action pro-cart" style="width: 100%;">
                                            <a title="Add To Cart" href="<?=base_url('produk/detail_produk/' . $v_p['id_produk']);?>"><i class="fa fa-eye"></i> Detail Produk</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content text-center">
                                    <h3><a href="<?=base_url('produk/detail_produk/' . $v_p['id_produk']);?>"><?=$v_p['namaproduk']?></a></h3>
                                    <div class="product-price">
                                        <span>IDR <?=$v_p['harga']?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="sidebar-style mr-30">
                    <div class="sidebar-widget">
                        <h4 class="pro-sidebar-title">Cari Produk </h4>
                        <div class="pro-sidebar-search mb-50 mt-15">
                            <form class="pro-sidebar-search-form" method="get" action="<?php echo base_url().'index.php/web/produk/' ?>">
                                <input type="text" name="cari" placeholder="Search here...">
                                <button>
                                    <i class="pe-7s-search"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="sidebar-widget kategori">
                        <h4 class="pro-sidebar-title">Kategori Produk</h4>
                        <div class="sidebar-widget-list">
                            <ul>
                                <?php foreach ($data_kategori as $v_k):?>
                                    <li>
                                        <div class="sidebar-widget-list-left">
                                            <a href="<?=base_url('produk'.'/'.$v_k['id_kategori']);?>"><?=$v_k['kategori']?></a>
                                        </div>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>