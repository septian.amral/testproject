<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="<?=base_url()?>">Beranda</a>
                </li>
                <li class="active"><?= $aktif ?> </li>
            </ul>
        </div>
    </div>
</div>
<div class="contact-area pt-100 pb-100">
    <div class="container">
        <div class="custom-row-2">
            <div class="col-lg-5 col-md-5">
                <div class="contact-info-wrap">
                    <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="contact-info-dec">
                            <p>0813-2566-1333</p>
                            <p>021-1234567</p>
                        </div>
                    </div>
                    <!-- <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="contact-info-dec">
                            <p><a href="#">urname@email.com</a></p>
                            <p><a href="#">urwebsitenaem.com</a></p>
                        </div>
                    </div> -->
                    <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="contact-info-dec">
                            <p>Kutoharjo, Kec. Rembang, </p>
                            <p>Kabupaten Rembang, Jawa Tengah 59211</p>
                        </div>
                    </div>
                    <div class="contact-social text-center">
                        <h3>Follow Us</h3>
                        <ul>
                            <li><a href="https://www.facebook.com/uwwawa" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/ahzastore.rbg/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-7">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15850.099423034218!2d111.3463569!3d-6.70561!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf1fc886e63747452!2sAHZA+STORE!5e0!3m2!1sid!2sid!4v1562280430969!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>
                <!-- <div class="contact-form">
                    <div class="contact-title mb-30">
                        <h2>Get In Touch</h2>
                    </div>
                    <form class="contact-form-style" id="contact-form" action="assets/php/mail.php" method="post">
                        <div class="row">
                            <div class="col-lg-6">
                                <input name="name" placeholder="Name*" type="text">
                            </div>
                            <div class="col-lg-6">
                                <input name="email" placeholder="Email*" type="email">
                            </div>
                            <div class="col-lg-12">
                                <input name="subject" placeholder="Subject*" type="text">
                            </div>
                            <div class="col-lg-12">
                                <textarea name="message" placeholder="Your Massege*"></textarea>
                                <button class="submit" type="submit">SEND</button>
                            </div>
                        </div>
                    </form>
                    <p class="form-messege"></p>
                </div> -->
            </div>
        </div>
    </div>
</div>
