<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li class="active">Shop </li>
            </ul>
        </div>
    </div>
</div>
<div class="shop-area pt-95 pb-100">
    <div class="container">
        <div class="row flex-row-reverse">
            <div class="col-lg-9">
                <div class="shop-top-bar">
                   <!--  <div class="select-shoing-wrap">
                        <div class="shop-select">
                            <select>
                                <option value="">Sort by newness</option>
                                <option value="">A to Z</option>
                                <option value=""> Z to A</option>
                                <option value="">In stock</option>
                            </select>
                        </div>
                        <p>Showing 1–12 of 20 result</p>
                    </div> -->
                    <div class="shop-tab nav">
                        <a class="active" href="#shop-1" data-toggle="tab">
                            <i class="fa fa-table"></i>
                        </a>
                        <a href="#shop-2" data-toggle="tab">
                            <i class="fa fa-list-ul"></i>
                        </a>
                    </div>
                </div>
                <div class="shop-bottom-area mt-35">
                    <div class="tab-content jump">
                        <div id="shop-1" class="tab-pane active">
                            <div class="row">
                                <?php foreach($data_produk as $v_produk): ?>
                                <div class="col-xl-4 col-md-6 col-lg-6 col-sm-6">
                                    <div class="product-wrap mb-25 scroll-zoom">
                                        <div class="product-img">
                                            <a href="#">
                                                <img class="default-img" src="assets/img/product/pro-1.jpg" alt="">
                                                <img class="hover-img" src="assets/img/product/pro-1-1.jpg" alt="">
                                            </a>
                                            <!-- <span class="pink">-10%</span> -->
                                            <div class="product-action">
                                                <div class="pro-same-action pro-wishlist">
                                                    <a title="Wishlist" href="#"><i class="pe-7s-like"></i></a>
                                                </div>
                                                <div class="pro-same-action pro-cart">
                                                    <a title="Add To Cart" href="<?=base_url('produk/detail_produk/' . $v_produk['id_produk']);?>"><i class="pe-7s-cart"></i> Lihat Rinci</a>
                                                </div>
                                                <div class="pro-same-action pro-quickview">
                                                    <a title="Quick View" href="#" data-toggle="modal" data-target="#exampleModal"><i class="pe-7s-look"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-content text-center">
                                            <h3><a href="<?=base_url('produk/detail_produk/' . $v_produk['id_produk']);?>"><?=$v_produk['namaproduk']?></a></h3>
                                            <div class="product-price">
                                                <span>IDR <?=$v_produk['harga']?></span>
                                                <!-- <span class="old">$ 60.00</span> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div id="shop-2" class="tab-pane">
                                <?php foreach($data_produk as $v_produk): ?>

                            <div class="shop-list-wrap mb-30">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-5 col-md-5 col-sm-6">
                                        <div class="product-wrap">
                                            <div class="product-img">
                                                <a href="#">
                                                    <img class="default-img" src="assets/img/product/pro-1.jpg" alt="">
                                                    <img class="hover-img" src="assets/img/product/pro-1-1.jpg" alt="">
                                                </a>
                                                <span class="pink">-10%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-8 col-lg-7 col-md-7 col-sm-6">
                                        <div class="shop-list-content">
                                            <h3><a href="#"><?=$v_produk['namaproduk']?></a></h3>
                                            <div class="product-list-price">
                                                <span>IDR <?=$v_produk['harga']?></span>
                                                <!-- <span class="old">$ 90.00</span> -->
                                            </div>
                                            <?=$v_produk['deskripsi']?>
                                            <div class="shop-list-btn btn-hover">
                                                <a href="<?=base_url('produk/detail_produk/' . $v_produk['id_produk']);?>">LIHAT RINCI</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <?php endforeach; ?>

                        </div>
                    </div>
                    <!-- <div class="pro-pagination-style text-center mt-30">
                        <ul>
                            <li><a class="prev" href="#"><i class="fa fa-angle-double-left"></i></a></li>
                            <li><a class="active" href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a class="next" href="#"><i class="fa fa-angle-double-right"></i></a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
            <div class="col-lg-3">
                <div class="sidebar-style mr-30">
<!--                     <div class="sidebar-widget">
                        <h4 class="pro-sidebar-title">Search </h4>
                        <div class="pro-sidebar-search mb-50 mt-25">
                            <form class="pro-sidebar-search-form" action="#">
                                <input type="text" placeholder="Search here...">
                                <button>
                                    <i class="pe-7s-search"></i>
                                </button>
                            </form>
                        </div>
                    </div> -->
                     <div class="sidebar-widget kategori">
                        <h4 class="pro-sidebar-title">Kategori Produk</h4>
                        <div class="sidebar-widget-list">
                            <ul>
                                <?php foreach($data_kategori as $v_kategori):?>
                                <li>
                                    <div class="sidebar-widget-list-left">
                                        <input type="button"> <a href="<?=base_url('produk/'.$v_kategori['id_kategori']);?>"><?=$v_kategori['kategori']?> </a> 
                                    </div>
                                </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>