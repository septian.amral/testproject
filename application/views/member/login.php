<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="">Beranda</a>
                </li>
                <li class="active">LOGIN</li>
            </ul>
        </div>
    </div>
</div>
<div class="login-register-area pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                <div class="login-register-wrapper">
                    <div class="login-register-tab-list nav">
                        <a class="active" data-toggle="tab" href="#lg1">
                            <h4> login </h4>
                        </a>
                        <!-- <a data-toggle="tab" href="#lg2">
                            <h4> register </h4>
                        </a> -->
                    </div>
                    <?=validation_errors();?>
                    <?php if ($this->session->flashdata('pesan')) { ?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                          <strong>Perhatian!</strong> <?=$this->session->flashdata('pesan');?>.
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php }?>
                <div class="tab-content">
                    <div id="lg1" class="tab-pane active">
                        <div class="login-form-container">
                            <div class="login-register-form">
                                <?= form_open('member/masuk'); ?>
                                <input type="text" name="username" placeholder="Username">
                                <input type="password" name="password" placeholder="Password">
                                <div class="button-box">
                                    <div class="login-toggle-btn">
                                                <!-- <input type="checkbox">
                                                    <label>Remember me</label> -->
                                                    <!-- <a href="#">Forgot Password?</a> -->
                                                </div>
                                                <button type="submit"><span>Masuk</span></button>
                                            </div>
                                            <?= form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>