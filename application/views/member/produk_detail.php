<?php foreach ($data_produk as $v_p): ?>
    <div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
        <div class="container">
            <div class="breadcrumb-content text-center">
                <ul>
                    <li>
                        <a href="<?=base_url();?>">Home</a>
                    </li>
                    <li class="active">Shop Details </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="shop-area pt-100 pb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="product-details">
                        <div class="product-details-img">
                            <div class="tab-content jump">
                                <div id="shop-details-1" class="tab-pane large-img-style">
                                    <img height="570" width="680" src="<?=base_url('gambar/'.$v_p['gambar']);?>" alt="">
                                    <div class="img-popup-wrap">
                                        <a class="img-popup" href="<?=base_url('gambar/'.$v_p['gambar']);?>"><i class="pe-7s-expand1"></i></a>
                                    </div>
                                </div>
                                <div id="shop-details-2" class="tab-pane active large-img-style">
                                    <img  height="570" width="680" src="<?=base_url('gambar/'.$v_p['gambar2']);?>" alt="">
                                    <div class="img-popup-wrap">
                                        <a class="img-popup"  href="<?=base_url('gambar/'.$v_p['gambar2']);?>"><i class="pe-7s-expand1"></i></a>
                                    </div>
                                </div>
                                <div id="shop-details-3" class="tab-pane large-img-style">
                                    <img height="570" width="680" src="<?=base_url('gambar/'.$v_p['gambar3']);?>" alt="">
                                    <div class="img-popup-wrap">
                                        <a class="img-popup"  href="<?=base_url('gambar/'.$v_p['gambar3']);?>"><i class="pe-7s-expand1"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="shop-details-tab nav">
                                <a class="shop-details-overly" href="#shop-details-1" data-toggle="tab">
                                    <img height="144" width="144" src="<?=base_url('gambar/'.$v_p['gambar']);?>" alt="">
                                </a>
                                <a class="shop-details-overly active" href="#shop-details-2" data-toggle="tab">
                                    <img height="144" width="144" src="<?=base_url('gambar/'.$v_p['gambar2']);?>" alt="">
                                </a>
                                <a class="shop-details-overly" href="#shop-details-3" data-toggle="tab">
                                    <img  height="144" width="144" src="<?=base_url('gambar/'.$v_p['gambar3']);?>" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product-details-content ml-70">
                    <?php if($this->session->flashdata('sukses')){ ?>
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Success!</strong> <?php echo $this->session->flashdata('sukses'); ?>
                            </div>

                        <?php } else if($this->session->flashdata('gagal')){  ?>

                            <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('gagal'); ?>
                        </div>

                    <?php } ?>
                        <?php echo form_open_multipart('keranjang/tambah_keranjang', 'id="tambah_keranjang"');?>
                        <?=form_hidden('id_produk', $v_p['id_produk']);?>
                        <?=form_hidden('harga', $v_p['harga']);?>
                        <?=form_hidden('namaproduk', $v_p['namaproduk']);?>
                        <h2><?=$v_p['namaproduk']?></h2>
                        <div class="product-details-price">
                            <span>IDR <?=number_format($v_p['harga'], 2)?> </span>
                        </div>
                        <p><?=$v_p['deskripsi']?></p>
                        <hr class="dtail">
                        <h4>Form Pesan Produk</h4>
                        <div class="pro-details-quality">
                            <div class="form-control">
                                <label>
                                    Quantity yang ingin dipesan*
                                </label>
                                <input class="form-control" type="number" name="jumlah_produk" placeholder="0" required="true">
                            </div>
                        </div>

                        <div class="pro-details-quality">
                            <div class="form-control">
                                <label>Spesifikasi Cetakan yang Anda inginkan*</label>
                                <textarea name="deskripsi" required="true"></textarea>
                            </div>
                        </div>
                        <div class="pro-details-quality">
                            <div class="form-control">
                                <input type="file" id="files" name="files[]" multiple="multiple" accept="image/*" />
                            </div>
                        </div>
                        
                        <div class="pro-details-quality">
                           <div class="form-control">
                            <input class="btn btn-success" type="submit" name="tambahkekeranjang" value="Proses Pesanan">
                        </div>
                    </div>
                    <?=form_close();?>
                    <div class="pro-details-meta">
                        <span>Kategori :</span>
                        <ul>
                            <li><a href="#"><?=$v_p['kategori']?></a></li>
                        </ul>
                    </div>
                    <div class="pro-details-social">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endforeach;?>
<div class="related-product-area pb-95">
    <div class="container">
        <div class="section-title text-center mb-50">
            <!-- <h2>Related products</h2> -->
        </div>
        <div class="related-product-active owl-carousel">
            <?php foreach ($produk_lain as $v_pl): ?>

                <div class="product-wrap">
                    <div class="product-img">
                        <a href="#">
                            <img class="default-img" src="assets/img/product/pro-1.jpg" alt="">
                            <img class="hover-img" src="assets/img/product/pro-1-1.jpg" alt="">
                        </a>
                        <span class="pink">-10%</span>
                        <div class="product-action">
                            <div class="pro-same-action pro-wishlist">
                                <a title="Wishlist" href="#"><i class="pe-7s-like"></i></a>
                            </div>
                            <div class="pro-same-action pro-cart">
                                <a title="Add To Cart" href="#"><i class="pe-7s-cart"></i> Lihat Rinci</a>
                            </div>
                            <div class="pro-same-action pro-quickview">
                                <a title="Quick View" href="#" data-toggle="modal" data-target="#exampleModal"><i class="pe-7s-look"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="product-content text-center">
                        <h3><a href="product-details.html"><?=$v_pl['namaproduk']?></a></h3>
                        <div class="product-rating">
                            <i class="fa fa-star-o yellow"></i>
                            <i class="fa fa-star-o yellow"></i>
                            <i class="fa fa-star-o yellow"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                        <div class="product-price">
                            <span>IDR <?=number_format($v_pl['harga'], 2)?> </span>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>