<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- 

<div class="slider-area">
    <div class="slider-active owl-carousel nav-style-1">
        <div class="single-slider single-slider-10 slider-height-8" style="background-image: url('<?= base_url('assets_fe')?>/images/Slider01.jpg'); ">
        </div>
        <div class="single-slider single-slider-10 slider-height-8" style="background-image: url('<?= base_url('assets_fe')?>/images/Slider01.jpg'); ">
        </div>
    </div>
</div> 
 -->
<style type="text/css">
.carousel{
    background: #2f4357;
}
.carousel .item img{
    margin: 0 auto; /* Align slide image horizontally center */
    height: 480px;
    width:100%;
    object-fit: cover;
}
.bs-example{
    /*margin: 20px;*/
}
.bs-example .btn-slider-go {
    padding: 10px 20px;
    /*font-weight: 600;*/
    background-color: #f4581b;
    color: #fff;
    margin-top: 10px;
}
.bs-example p.tanggal-go {
    color: #fff;
    font-weight: 800;
    margin-bottom: 3%;
}
.carousel-caption {
    text-align: left;
}
@media screen and (max-width: 767px) {
    .carousel .item img {
        height: 300px;
    }
}
@media screen and (min-width: 768px){
    .bs-example .carousel-caption {
        left: 10%;
        padding-bottom: 60px;
    }
}
</style>
<div class="bs-example">
    <div class="">
        <div class=''>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Carousel indicators -->
                <!-- <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <?php $no = 0; foreach ($berita as $info) {
                    ?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo ++$no; ?>"></li>
                    <?php } ?>
                </ol>    -->
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img class="single-slider single-slider-10 slider-height-8" src="<?= base_url('flone')?>/assets/images/Slider-Kartu-lebaran.png" alt="Slider Percetakan Ahza">
                            <div class="carousel-caption">
                                <h3></h3>
                                <p></p>
                            </div>
                    </div>
                    <div class="item">
                        <img class="single-slider single-slider-10 slider-height-8" src="<?= base_url('flone')?>/assets/images/Slider-Kartu-Nama.png" alt="Slider Percetakan Ahza">
                            <div class="carousel-caption">
                                <h3></h3>
                                <p></p>
                            </div>
                    </div>
                    <div class="item">
                        <img class="single-slider single-slider-10 slider-height-8" src="<?= base_url('flone')?>/assets/images/slide-undangan.png" alt="Slider Percetakan Ahza">
                            <div class="carousel-caption">
                                <h3></h3>
                                <p></p>
                            </div>
                    </div>
                    <!-- <?php foreach ($berita as $info) {
                    ?>
                        <div class="item">
                            <img class="single-slider single-slider-10 slider-height-8"  src="<?=base_url('images');?>/berita/<?php echo $info->foto ?>" alt="First Slide">
                            <div class="carousel-caption">
                              <h3><?php echo $info->judul ?></h3>
                              <p class="tanggal-go"><?php echo fdate($info->tgl_upload, "DDMMYYYY") ?></p>
                              <p><a href="<?=site_url()?>/web/berita_detail/<?php echo md5($info->id_artikel) ?>" class="animated btn-slider-go">Selengkapnya</a></p>
                            </div>
                        </div>
                    <?php } ?> -->
                </div>
                <!-- Carousel controls -->
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="suppoer-area pt-50 pb-60 tentang-kami">
    <div class="container">
        <div class="jalan">
            <div class="tkiri">
                HIGHLIGHT                        
            </div>
            <div class="tkanan">
                <marquee>
                     Selamat Datang di Website Percetakan Ahza <i class="fa fa-circle"></i>
                </marquee>  
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="support-wrap-2 mb-30 support-shape text-center">
                    <div class="support-content-2">
                        <img class="animated" src="<?=base_url('flone');?>/assets/images/shipped.png" alt="">
                        <h5 style="font-family:Poppins", sans-serif;">Pengiriman Aman</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="support-wrap-2 mb-30 support-shape text-center">
                    <div class="support-content-2">
                        <img class="animated" src="<?=base_url('flone');?>/assets/images/customer-service.png" alt="">
                        <h5 style="font-family:Poppins", sans-serif;">Support Konsultasi</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="support-wrap-2 mb-30 text-center">
                    <div class="support-content-2">
                        <img class="animated" src="<?=base_url('flone');?>/assets/images/reliability.png" alt="">
                        <h5 style="font-family:Poppins", sans-serif;">Terpercaya</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="product-area pt-70 pb-60">
    <div class="container">
        <div class="section-title text-center">
            <h2>PRODUK TERBARU</h2>
        </div>
        <div class="product-tab-list nav pt-20 pb-55 text-center">
        </div>
        <div class="tab-content jump">
            <div class="tab-pane active" id="product-1">
                <div class="row">
                    <?php
                    foreach ($data_produk as $v_p):
                        ?>
                        <div class="col-xl-3 col-md-6 col-lg-4 col-sm-6">
                            <div class="product-wrap mb-25 scroll-zoom">
                                <div class="product-img">
                                    <a href="<?=base_url('produk/detail_produk/' . $v_p['id_produk']);?>">
                                        <img class="default-img" src="<?=base_url('flone');?>/assets/img/product/pro-8.jpg" alt="">
                                        <img class="hover-img" src="<?=base_url('flone');?>/assets/img/product/pro-6.jpg" alt="">
                                    </a>
                                    <span class="purple"><?=$v_p['kategori']?></span>
                                    <div class="product-action">
                                        <div class="pro-same-action pro-cart" style="width: 100%;">
                                            <a title="Add To Cart" href="<?=base_url('produk/detail_produk/' . $v_p['id_produk']);?>"><i class="fa fa-eye"></i> Detail Produk</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-content text-center">
                                    <h3><a href="<?=base_url('produk/detail_produk/' . $v_p['id_produk']);?>"><?=$v_p['namaproduk']?></a></h3>
                                    <div class="product-price">
                                        <span>IDR <?=$v_p['harga']?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div> -->