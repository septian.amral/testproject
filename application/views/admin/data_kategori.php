              <!-- Begin Page Content -->
              <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800"><?php echo $sub_judul; ?> </h1>
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><?php echo $sub_judul; ?></h6>
                  </div>
                  <?php if($this->session->flashdata('sukses')){ ?>
                    <div class="alert alert-success">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong>Success!</strong> <?php echo $this->session->flashdata('sukses'); ?>
                    </div>

                  <?php } else if($this->session->flashdata('gagal')){  ?>

                    <div class="alert alert-danger">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong>Error!</strong> <?php echo $this->session->flashdata('gagal'); ?>
                    </div>

                  <?php } ?>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Kategori</th>
                            <!-- <th>Deskripsi</th> -->
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th>No</th>
                            <th>Nama Kategori</th>
                            <!-- <th>Deskripsi</th> -->
                            <th>Aksi</th>
                          </tr>
                        </tfoot>
                        <tbody>
                          <?php $no = 1;foreach ($data_kategori as $v_kategori): ?>
                          <tr>

                            <td><?=$no++?></td>
                            <td><?=$v_kategori['kategori'];?></td>
                            <!-- <td><?=$v_kategori['deskripsi'];?></td> -->
                            <td class="text-center">
                              <a href="<?= base_url('kategori/hapus_kategori/'.$v_kategori["id_kategori"]); ?>" class="btn btn-danger btn-circle btn-sm">
                                <i class="fas fa-trash"></i>
                              </a>
                              <a href="<?= base_url('kategori/ubah_kategori/'.$v_kategori["id_kategori"]); ?>" class="btn btn-primary btn-circle btn-sm">
                                <i class="fas fa-pen"></i>
                              </a></td>
                            </tr>
                          <?php endforeach;?>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
              <!-- /.container-fluid -->