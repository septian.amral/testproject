              <!-- Begin Page Content -->
              <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Menampilkan <?php echo $sub_judul; ?> </h1>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><?php echo $sub_judul; ?></h6>
                  </div>
                  <?php if($this->session->flashdata('sukses')){ ?>
                    <div class="alert alert-success">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong>Success!</strong> <?php echo $this->session->flashdata('sukses'); ?>
                    </div>

                  <?php } else if($this->session->flashdata('gagal')){  ?>

                    <div class="alert alert-danger">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong>Error!</strong> <?php echo $this->session->flashdata('gagal'); ?>
                    </div>

                  <?php } ?>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Level</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Level</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Aksi</th>
                          </tr>
                        </tfoot>
                        <tbody>
                          <?php $no = 1;foreach ($data_admin as $v_admin): ?>
                          <tr>

                            <td><?=$no++?></td>
                            <td><?=$v_admin['nama'];?></td>
                            <td><?=$v_admin['username'];?></td>
                            <td><?=$v_admin['password'];?></td>
                            <td><?=($v_admin['level'] == 1 ? "Admin" : "Owner");?></td>
                            <td class="text-center">
                              <a href="<?= base_url('admin/hapus_admin/'.$v_admin["id_pengguna"].'/'.$v_admin["status"]); ?>" class="btn btn-<?=($v_admin['status'] == 1) ? 'success btn btn-sm" data-toggle="tooltip" title="Status Aktif"' : 'danger btn btn-sm" data-toggle="tooltip" title="Status Tidak Aktif"';?>
                                    ">
                                Aktif
                              </a>
                            </td>
                            <td class="text-center">
                              <a href="<?= base_url('admin/ubah_admin/'.$v_admin["id_pengguna"]); ?>" class="btn btn-primary btn-circle btn-sm">
                                <i class="fas fa-pen"></i>
                              </a></td>
                            </tr>
                          <?php endforeach;?>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
              <!-- /.container-fluid -->