<!--
    Begin Page Content
-->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">
        <?=$judul;?>
    </h1>
    <!-- Default Card Example -->
    <div class="card mb-4">
        <div class="card-header">
            <?=$sub_judul?>
        </div>
        <div class="card-body">
            <?=validation_errors();?>
            <?=form_open('admin/simpan_ubah', 'id="simpan_ubah"');?>
            <?php  foreach ($data_admin as $v_admin): ?>
                <?=form_hidden('id_pengguna', $v_admin['id_pengguna']);?>

                <div class="form-group">
                    <label for="usr">
                        Nama:
                    </label>
                    <input type="text" class="form-control" value="<?=$v_admin['nama']?>" id="nama" name="nama" placeholder="Masukan nama lengkap..." autocomplete="false"  required="true" />
                </div>
                <div class="form-group">
                    <label for="usr">
                        Username:
                    </label>
                    <input type="text" class="form-control" value="<?=$v_admin['username']?>"  id="usename" name="username" placeholder="Username" autocomplete="false" required="true/">
                </div>
                <div class="form-group">
                    <label for="usr">
                        Password:
                    </label>
                    <input type="password" class="form-control" value="<?=$v_admin['password']?>"  id="password" name="password" placeholder="Password" autocomplete="false" required="true"/>
                </div>
                <div class="form-group">
                    <label for="level">
                        Level:
                    </label>
                    <select  class="form-control" id="level" name="level">
                      <optgroup label="Pilih Level">
                        <option value="0" <?= set_select('level', '0', ($v_admin['level'] == '0')); ?>>Owner</option>
                        <option value="1" <?= set_select('level', '1', ($v_admin['level'] == '1')); ?>>Admin/Kasir</option>
                    </optgroup>
                </select>
            </div>
            <div class="text-right">

                <div class="btn-group">
                    <button type="reset" class="btn btn-danger btn-user btn-block">
                        Batal
                    </button>
                </div>
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        Ubah Admin
                    </button>
                </div>
            </div>
        <?php endforeach; ?>

    </form>
</div>
</div>
</div>
<!-- /.container-fluid -->
