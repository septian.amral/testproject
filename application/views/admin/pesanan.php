<div class="profil-area pt-95 pb-100">
    <div class="container">
        <div class="row profile">
            <div class="col-md-12">
                <div class="shadow1">
                    <div class="card-body">
                        <div class="profile-content">
                            <?= $pagination ?>

                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th class="text-center">
                                                User Pelanggan
                                            </th>
                                            <th class="text-center">
                                                Tgl Pemesanan
                                            </th>
                                            <th class="text-center">
                                                No Pemesanan
                                            </th>
                                            <th class="text-center">
                                                Jumlah Barang (Pcs)
                                            </th>
                                            <th class="text-center">
                                                Nominal
                                            </th>
                                            <th class="text-center">
                                                Status
                                            </th>
                                            <th class="text-center">
                                                Bukti Transfer
                                            </th>
                                            <th class="text-center">
                                                Aksi
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no = 1; foreach($list_invoice as $items): ?>
                                        <tr>
                                            
                                                <td class="text-center">
                                                    <?=($offset + $no++) ?>
                                                </td>
                                                <td class="text-center">
                                                    <?=$items['username'] ?>
                                                </td>
                                                <td class="text-center">
                                                    <?=$items['created_date'] ?>
                                                </td>
                                                <td class="text-center">
                                                    <?= $items['invoice_no'] ?>
                                                </td>
                                                <td class="text-center">
                                                    <?=$items['jumlah'] ?>
                                                </td>
                                                <td class="text-center">
                                                    <?=$items['totalbayar']?>
                                                </td>
                                                <td class="text-center">
                                                <?php
                                                    if ($items['status'] == 1){
                                                        echo 'Belum Melakukan Pembayaran';
                                                    } else {
                                                        echo 'Sudah Dibayar';
                                                    }
                                                ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php if (isset($items['buktipem'])) { ?>
                                                        <a  target="_blank" href="<?= UPLOAD_IMAGE_LOCATION.$items['buktipem']?>">
                                                            <i class="fas fa-eye"></i>
                                                        </a>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center">
                                                    <a href="<?= base_url() ?>pesanan/pesanan_detail_admin/<?= $items['id'] ?>">Detail </a>
                                                    <br>
                                                    <a data-toggle="modal" onclick="bind_table(this)" data-target="#confirm_bukti"
                                                        value="<?=$items['id']?>" href="">Confirm</a>
                                                </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>

                            <?= $pagination ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="confirm_bukti">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open('pesanan/confirm');?>
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi Bukti Transfer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <input type="hidden" id="id_pesanan" name="id_pesanan" value="" />
                Apakah Anda sudah yakin ?
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <!--
                <a onclick="peringtan()" type="button" class="btn btn-success" data-dismiss="modal">Oke</a>
                <a type="button" class="btn btn-danger" data-dismiss="modal">Batal</a>
                -->
                <input type="submit" class="btn btn-success" value="Ya" />
                <input type="button" class="btn btn-danger" value="Cancel" data-dismiss="modal"/>
            </div>
            </form>
        </div>
    </div>
</div>

<script src="<?=base_url('assets/vendor/jquery/jquery.min.js');?>"></script>
<script type="text/javascript">
    function bind_table(elem) {
        var temp = $(elem).parent().parent().find('td').first().next().next().next().text().trim();
        var res = temp.split("-");
        var id_pesanan = parseInt(res[1]);

        $('#id_pesanan').val(elem.getAttribute('value'));
    }    
</script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>

