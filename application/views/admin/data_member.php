              <!-- Begin Page Content -->
              <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Menampilkan <?php echo $sub_judul; ?> </h1>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                  <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><?php echo $sub_judul; ?></h6>
                  </div>
                  <?php if($this->session->flashdata('sukses')){ ?>
                    <div class="alert alert-success">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong>Success!</strong> <?php echo $this->session->flashdata('sukses'); ?>
                    </div>

                  <?php } else if($this->session->flashdata('gagal')){  ?>

                    <div class="alert alert-danger">
                      <a href="#" class="close" data-dismiss="alert">&times;</a>
                      <strong>Error!</strong> <?php echo $this->session->flashdata('gagal'); ?>
                    </div>

                  <?php } ?>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Telepon</th>
                            <th class="text-center">Tanggal Gabung</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Aksi</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Telepon</th>
                            <th class="text-center">Tanggal Gabung</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Aksi</th>
                          </tr>
                        </tfoot>
                        <tbody>
                          <?php $no = 1;foreach ($data_member as $v_member): ?>
                          <tr>
                            <td class="text-center"><?=$no++?></td>
                            <td><?=$v_member['nama'];?></td>
                            <td><?=$v_member['email'];?></td>
                            <td><?=$v_member['telepon'];?></td>
                            <td class="text-center"><?=$v_member['tglgabung'];?></td>
                            <td class="text-center"><?=$v_member['status'];?></td>
                            <td class="text-center">

                              <a href="<?= base_url('member/detail_member/'.$v_member["id_pelanggan"]); ?>" class="btn btn-danger btn-circle btn-sm" data-toggle="tooltip" title="Detail">
                                <i class="fas fa-eye"></i>
                              </a> 
                              <a href="" class="btn btn-danger btn-circle btn-sm" data-toggle="modal" data-target=".ubahstatus">
                                <i class="fa fa-pencil"></i>
                              </a> 
                              <a href="<?= base_url('member/hapus_member/'.$v_member["id_pelanggan"]); ?>" class="btn btn-danger btn-circle btn-sm">
                                <i class="fas fa-trash"></i>
                              </a>
                            </td>
                          </tr>
                        <?php endforeach;?>
                      </tr>
                    </tbody>
                  </table>

                  <div class="modal fade ubahstatus" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="mySmallModalLabel">Ubah Status</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                              <?= form_open('member/buat_member'); ?>
                                <select class="form-control" name="id_kategori" id="id_kategori">
                                    <optgroup label="Pilih Status">
                                        <?php foreach ($data_kategori as $vk): ?>
                                        <?="<option value='{$vk["id_kategori"]}'>" . $vk['kategori'] . "</option>"?>
                                        <?php endforeach;?>
                                    </optgroup>
                                </select>
                              <?= form_close(); ?>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                </div>
              </div>
            </div>

          </div>
              <!-- /.container-fluid -->