<!--
    Begin Page Content
-->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">
        <?=$judul;?>
    </h1>
    <!-- Default Card Example -->
    <div class="card mb-4">
        <div class="card-header">
            <?=$sub_judul?>
        </div>
        <div class="card-body">
          <?php echo validation_errors(); ?>
            <?=form_open('admin/tambah_admin', 'id="tambah_admin"');?>
                <div class="form-group">
                    <label for="usr">
                        Nama:
                    </label>
                    <input type="text" class="form-control " id="nama" name="nama" placeholder="Masukan nama lengkap..." autocomplete="false"  required="true" />
                </div>
                <div class="form-group">
                    <label for="usr">
                        Username:
                    </label>
                    <input type="text" class="form-control" id="usename" name="username" placeholder="Username" autocomplete="false" required="true/">
                </div>
                <div class="form-group">
                    <label for="usr">
                        Password:
                    </label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" autocomplete="false" required="true"/>
                </div>
                <div class="form-group">
                    <label for="level">
                        Level:
                    </label>
                    <select  class="form-control" id="level" name="level">
                      <optgroup label="Pilih Level">
                        <option value="0">Owner</option>
                        <option value="1">Admin/Kasir</option>
                      </optgroup>
                    </select>
                </div>
                <div class="text-right">

                <div class="btn-group">
                    <button type="reset" class="btn btn-danger btn-user btn-block">
                        Batal
                    </button>
                </div>
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        Buat Admin
                    </button>
                </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
