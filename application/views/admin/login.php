
<body style="background-image: url('<?=base_url("flone");?>/assets/images/bg-login.jpg'); background-size: cover; background-position: bottom;">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-4 col-lg-4 col-md-4">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <!-- <h1 class="h4 text-gray-900 mb-4">Selamat Datang!</h1> -->
                    <img alt="Logo Percetakan Ahza" src="<?= base_url('flone')?>/assets/images/logo-login.png" style="width: 90%;    margin-bottom: 15%;">
                  </div>
                  <?=form_open('admin/login', 'class="user"');?>
                  <?=validation_errors();?>
                  <?php if ($this->session->flashdata('pesan')) { ?>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                      <strong>Perhatian!</strong> <?=$this->session->flashdata('pesan');?>.
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                  <?php }?>
                  <div class="form-group">
                    <input type="text" class="form-control form-control-user" id="username" name="username" aria-describedby="emailHelp" placeholder="Masukan Username">
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Masukan Password">
                  </div>
                  <input type="submit" class="btn btn-primary btn-user btn-block" value="Login">
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>