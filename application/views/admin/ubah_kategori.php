<!--
    Begin Page Content
-->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">
        <?=$judul;?>
    </h1>
    <!-- Default Card Example -->
    <div class="card mb-4">
        <div class="card-header">
            <?=$sub_judul?>
        </div>
        <div class="card-body">
            <?=validation_errors();?>
            <?=form_open('kategori/simpan_ubah_kategori', 'id="simpan_ubah_kategori"');?>
            <?php foreach ($data_kategori as $v_kategori): ?>
                <?=form_hidden('id_kategori', $v_kategori['id_kategori']);?>
                <div class="form-group">
                    <label for="usr">
                        Nama Kategori:
                    </label>
                    <input type="text" class="form-control" value="<?=$v_kategori['kategori']?>" id="namakategori" name="namakategori" autocomplete="false" required="true" />
                </div>
                <!-- <div class="form-group">
                    <label for="usr">
                        Deskripsi:
                    </label>
                    <textarea name="deskripsi" class="form-control"><?=$v_kategori['deskripsi']?></textarea>
                </div> -->
                <div class="text-right">
                    <div class="btn-group">
                        <button type="reset" class="btn btn-danger btn-user btn-block">
                            Batal
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Ubah Kategori
                        </button>
                    </div>
                </div>
            <?php endforeach;?>
        </form>
    </div>
</div>
</div>
<!-- /.container-fluid -->
