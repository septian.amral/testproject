<!--Begin Page Content-->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">
        <?=$judul;?>
    </h1>
    <!-- Default Card Example -->
    <div class="card mb-4">
        <div class="card-header">
            <?=$sub_judul?>
        </div>
        <div class="card-body">
            <?=validation_errors();?>
            <?=form_open_multipart('produk_admin/simpan_ubah_produk', 'id="simpan_ubah_produk"');?>
            <?php foreach ($data_produk as $v_produk): ?>
            <?=form_hidden('id_produk', $v_produk['id_produk']);?>
                <div class="form-group">
                    <label for="usr">
                        Nama Produk:
                    </label>
                    <input type="text" class="form-control " id="namaproduk" name="namaproduk" value="<?=$v_produk['namaproduk']?>" autocomplete="false" required="true" />
                </div>
                <div class="form-group">
                    <label for="usr">
                        Deskripsi:
                    </label>
                    <textarea name="deskripsi" id="deskripsi" class="form-control"><?=$v_produk['deskripsi']?></textarea>
                </div>
                <div class="form-group">
                    <label for="kategori">
                        Kategori:
                    </label>
                    <select class="form-control" name="id_kategori" id="id_kategori">
                        <optgroup label="Pilih Kategori">
                            <?php foreach ($data_kategori as $vk): ?>
                                <?="<option value='{$vk["id_kategori"]}'>" . $vk['kategori'] . "</option>"?>
                            <?php endforeach;?>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group">
                    <label for="harga">
                        Harga :
                    </label>
                    <input type="number" class="form-control " id="harga" name="harga" value="<?=$v_produk['harga']?>" autocomplete="false" required="true" />
                </div>
                <div class="form-group">
                    <label for="harga">
                        Berat :
                    </label>
                    <input type="number" class="form-control " id="berat" name="berat" value="<?=$v_produk['berat']?>" autocomplete="false" required="true" />
                </div>
                <div class="form-group">
                    <label for="gambar1">
                        Gambar :
                    </label>
                    <input type="file" name="userfile[]" multiple="multiple" class="form-control-file" >
                    <?php if(!empty($v_produk['gambar'])):?>
                        <img src="<?=path_gambar.$v_produk['gambar']?>" class="img-thumbnail" height="250" width="250">
                    <?php endif;?>
                </div>
                <div class="form-group">
                    <label for="gambar2">
                        Gambar 2:
                    </label><br>
                    <?php if(!empty($v_produk['gambar2'])):?>
                        <img src="<?=path_gambar.$v_produk['gambar2']?>" class="img-thumbnail" height="250" width="250">
                    <?php endif;?>
                    <div class="form-group">
                        <label for="gambar3">
                            Gambar 3:
                        </label><br>
                        <?php if(!empty($v_produk['gambar3'])):?>
                            <img src="<?=path_gambar.$v_produk['gambar3']?>" class="img-thumbnail" height="250" width="250">
                        <?php endif;?>
                    </div>
                    <div class="text-right">

                        <div class="btn-group">
                            <button type="reset" class="btn btn-danger btn-user btn-block">
                                Batal
                            </button>
                        </div>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                Buat Produk
                            </button>
                        </div>
                    </div>
                <?php endforeach;?>
            </form>
        </div>
    </div>
</div>
<!-- /.container-fluid -->