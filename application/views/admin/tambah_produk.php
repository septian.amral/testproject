<!--Begin Page Content-->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">
        <?=$judul;?>
    </h1>
    <!-- Default Card Example -->
    <div class="card mb-4">
        <div class="card-header">
            <?=$sub_judul?>
        </div>
        <div class="card-body">
            <?php if($this->session->flashdata('sukses')){ ?>
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('sukses'); ?>
                </div>

            <?php } else if($this->session->flashdata('gagal')){  ?>

                <div class="alert alert-danger">
                  <a href="#" class="close" data-dismiss="alert">&times;</a>
                  <strong>Error!</strong> <?php echo $this->session->flashdata('gagal'); ?>
              </div>

          <?php } ?>
          <?=form_open_multipart('produk_admin/tambah_produk', 'id="tambah_produk"');?>
          <div class="form-group">
            <label for="namaproduk">
                Nama Produk:
            </label>
            <input type="text" class="form-control " id="namaproduk" name="namaproduk" placeholder="Masukan nama produk..." autocomplete="false" required="true" />
        </div>
        <div class="form-group">
            <label for="deskripsi">
                Deskripsi:
            </label>
            <textarea name="deskripsi" id="deskripsi" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="kategori">
                Kategori:
            </label>
            <select class="form-control" name="id_kategori" id="id_kategori">
                <optgroup label="Pilih Kategori">
                    <?php foreach ($data_kategori as $vk): ?>
                        <?="<option value='{$vk["id_kategori"]}'>" . $vk['kategori'] . "</option>"?>
                    <?php endforeach;?>
                </optgroup>
            </select>
        </div>
        <div class="form-group">
            <label for="harga">
                Harga :
            </label>
            <input type="number" class="form-control " id="harga" name="harga" placeholder="Masukan Harga produk..." autocomplete="false" required="true" />
        </div>
        <div class="form-group">
            <label for="harga">
                Berat :
            </label>
            <input type="number" class="form-control " id="berat" name="berat" placeholder="Masukan berat produk..." autocomplete="false" required="true" />
        </div>
        <div class="form-group">
            <label for="gambar">
                Gambar:
            </label>
            <input type="file" name="userfile[]" multiple="multiple" class="form-control-file" >
        </div>
        <div class="text-right">

            <div class="btn-group">
                <button type="reset" class="btn btn-danger btn-user btn-block">
                    Batal
                </button>
            </div>
            <div class="btn-group">
                <button type="submit" class="btn btn-primary btn-user btn-block">
                    Tambah Produk
                </button>
            </div>
        </div>
    </form>
</div>
</div>
</div>
<!-- /.container-fluid -->