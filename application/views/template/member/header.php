<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= $judul ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url('flone');?>/assets/images/fav.png">
    
    <!-- CSS
  ============================================ -->
   
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('flone/assets/css/bootstrap.min.css')?>">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="<?= base_url('flone/assets/css/icons.min.css')?>">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="<?= base_url('flone/assets/css/plugins.css')?>">
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="<?= base_url('flone/assets/css/style.css')?>">
    <!-- Main Style CSS Custom Vii-->
    <link rel="stylesheet" href="<?= base_url('flone/assets/css/custom.css')?>">

    <link rel="stylesheet" href="<?= base_url('flone/assets/css/invoice.css')?>">

    <link href="<?=base_url('flone');?>/plugins/select2/css/select2.min.css" rel="stylesheet" />

    <!-- Modernizer JS -->
    <script src="<?= base_url('flone')?>/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <style type="text/css">
        .select2-container, .select2-dropdown, .select2-search, .select2-results {
-webkit-transition: none !important;
-moz-transition: none !important;
-ms-transition: none !important;
-o-transition: none !important;
transition: none !important;
}
    </style>
</head>
