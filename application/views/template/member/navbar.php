<body>
    
    <header class="header-area header-in-container clearfix">
        <div class="header-bottom sticky-bar header-res-padding">
            <div class="container">
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-6 col-4">
                        <div class="logo">
                            <a href="<?=base_url()?>">
                                <img alt="Logo Percetakan Ahza" src="<?= base_url('flone')?>/assets/images/logo.png">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-10 col-lg-10 d-none d-lg-block">
                        <div class="main-menu">
                            <nav>
                                <ul>
                                    <li <?php if($aktif =='Beranda' ) { echo 'class="active"'; }?> >
                                        <a href="<?=base_url()?>">
                                            Beranda 
                                        </a>
                                    </li>
                                    <li <?php if($aktif =='Tentang Ahza' ) { echo 'class="active"'; }?> >
                                        <a href="<?=site_url()?>frontend/tentang_kami"> Tentang Ahza </a>
                                    </li>
                                    <li <?php if($aktif =='Produk' ) { echo 'class="active"'; }?> >
                                        <a href="<?=site_url()?>produk"> Produk </a>
                                    </li>
                                    <li <?php if($aktif =='Hubungi Kami' ) { echo 'class="active"'; }?> >
                                        <a href="<?=site_url()?>frontend/kontak"> Hubungi Kami </a>
                                    </li>
                                    <?php if(!empty($this->session->userdata('username'))): ?>
                                        <li class="daftar <?php if($aktif =='daftar-anggota' ) { echo 'active'; }?>">
                                        <a href="<?=base_url('profil')?>"> 
                                            <span>
                                                Akunku
                                            </span>
                                        </a>
                                    </li>
                                    <li class="masuk <?php if($aktif =='masuk-anggota' ) { echo 'active'; }?>" >
                                        <a href="<?=base_url('member/logout')?>"> 
                                            <span>
                                                Keluar
                                            </span>
                                        </a>
                                    </li>
                                    <?php else: ?>
                                        <li class="daftar <?php if($aktif =='daftar-anggota' ) { echo 'active'; }?>">
                                        <a href="<?=base_url('member/daftar')?>"> 
                                            <span>
                                                Daftar
                                            </span>
                                        </a>
                                    </li>
                                    <li class="masuk <?php if($aktif =='masuk-anggota' ) { echo 'active'; }?>" >
                                        <a href="<?=base_url('member/masuk')?>"> 
                                            <span>
                                                Masuk
                                            </span>
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="mobile-menu-area">
                    <div class="mobile-menu">
                        <nav id="mobile-menu-active">
                            <ul class="menu-overflow">
                                <li <?php if($aktif =='Beranda' ) { echo 'class="active"'; }?> >
                                    <a href="<?=base_url()?>">
                                        Beranda 
                                    </a>
                                </li>
                                <li <?php if($aktif =='Tentang Ahza' ) { echo 'class="active"'; }?> >
                                    <a href="<?=site_url()?>/frontend/tentang_kami"> Tentang Ahza </a>
                                </li>
                                <li <?php if($aktif =='Produk' ) { echo 'class="active"'; }?> >
                                    <a href="<?=site_url()?>/frontend/produk"> Produk </a>
                                </li>
                                <li <?php if($aktif =='Hubungi Kami' ) { echo 'class="active"'; }?> >
                                    <a href="<?=site_url()?>/frontend/kontak"> Hubungi Kami </a>
                                </li>
                                <li class="<?php if($aktif =='daftar-anggota' ) { echo 'active'; }?>">
                                    <a href="<?=base_url('member/daftar')?>"> 
                                        <span>
                                            Daftar
                                        </span>
                                    </a>
                                </li>
                                <li class="<?php if($aktif =='masuk-anggota' ) { echo 'active'; }?>" >
                                    <a href="<?=base_url('admin/login')?>"> 
                                        <span>
                                            Masuk
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>