<div class="profil-area pt-95 pb-100">
    <div class="container">
        
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="<?= base_url() ?>flone/assets/images/logo.png" style="width:100%; max-width:300px;">
                            </td>
                            
                            <td>
                                Invoice #: <?= $invoice['invoice_no'] ?><br>
                                Created: <?=date("Y-m-d",strtotime($invoice['created_date']))?><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Item
                </td>
                <td>
                    Jumlah
                </td>
                <td>
                    SubTotal
                </td>
            </tr>
            <?php foreach ($list_pesanan as $pesanan) { ?>
            <tr class="item">
                <td>
                    <?= $pesanan['namaproduk'] ?>
                </td>
                <td>
                    <?= $pesanan['jumlah'] ?>
                </td>
                <td>
                    Rp <?= $pesanan['jumlah'] * $pesanan['harga'] ?>
                </td>
            </tr>
            <?php } ?>
            
            <tr class="item last">
                <td>
                    Biaya Pengiriman <?= $invoice['metode_pengiriman'] ?>
                </td>
                <td>
                </td>
                <td>
                    Rp <?= $invoice['ongkir']?>
                </td>
            </tr>
            
            <tr class="total">
                <td></td>
                
                <td colspan="2">
                   Total: Rp <?= $pesanan['totalbayar'] ?>
                </td>
            </tr>
        </table>
    </div>
        
    </div>
</div>