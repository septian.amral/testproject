<div class="profil-area pt-95 pb-100">
    <div class="container">
        <div class="row profile">
            <div class="col-md-3">
                <div class="profile-sidebar">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic" style="text-align: center;">
                        <img src="<?= base_url() ?>images/u1.png" class="img-responsive" alt="">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            <?=$this->session->userdata('username')?>
                        </div>
                        <!-- <div class="profile-usertitle-job">
                            Developer
                        </div> -->
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <!-- <div class="profile-userbuttons">
                        <a href="<?=base_url('produk')?>" class="btn btn-success btn-sm">Pesan</a>
                        <a href="<?=base_url('member/logout')?>" class="btn btn-danger btn-sm">Keluar</a>
                    </div> -->
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="<?php if ($aktif == 'Profil') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil') ?>">
                                <i class="glyphicon glyphicon-home"></i>
                                Overview </a>
                            </li>
                            <li class="<?php if ($aktif == 'Pengaturan Profil') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil/pengaturan') ?>">
                                <i class="glyphicon glyphicon-user"></i>
                                Profil </a>
                            </li>
                            <li class="<?php if ($aktif == 'Pesanan Saya') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil/pesanan') ?>">
                                <i class="glyphicon glyphicon-flag"></i>
                                Pesanan Saya </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>
            <div class="col-md-9">
                <div class="shadow1">
                    <div class="card-body">
                        <div class="profile-content">
                            <div role="alert" class="alert alert-warning">
                            	Atur info rekening Anda untuk meningkatkan <strong>keamanan dan kemudahan transaksi</strong> Anda di Percetakan Ahza. 
                            </div>
                            <form>
                            	<div class="form-group">
                                        <label for="" class="col-xl-12 col-md-12 form-control-label">Nama Bank</label>
                                        <div class="col-xl-12 col-md-12">
                                            <select class="form-control" id="select">
                                                        <option>BCA</option>
                                                        <option>Mandiri</option>
                                            </select>
                                        </div>
                                </div>
                            	<div class="form-group">
                                        <label for="" class="col-xl-12 col-md-12 form-control-label">Nomor Kartu Debit/ Kredit</label>
                                        <div class="col-xl-12 col-md-12">
                                            <input class="form-control" id="" placeholder="Nomor Kartu Debit/ Kredit" type="text">
                                        </div>
                                </div>
                            	<div class="form-group">
                                        <label for="" class="col-xl-12 col-md-12 form-control-label">Nama Pemilik</label>
                                        <div class="col-xl-12 col-md-12">
                                            <input class="form-control" id="" placeholder="Nama Pemilik" type="text">
                                        </div>
                                </div>
                            	<div class="form-group">
                                        <label for="" class="col-xl-12 col-md-12 form-control-label">Masa Berlaku Kartu</label>
                                        <div class="col-xl-12 col-md-12">
                                            <input class="form-control" id="" placeholder="Masa Berlaku Kartu" type="text">
                                        </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>