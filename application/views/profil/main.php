<div class="profil-area pt-95 pb-100">
    <div class="container">
    	<div class="row profile">
            <div class="col-md-3">
                <div class="profile-sidebar">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic" style="text-align: center;">
                        <img src="<?= base_url() ?>images/u1.png" class="img-responsive" alt="">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            <?=$this->session->userdata('username')?>
                        </div>
                        <!-- <div class="profile-usertitle-job">
                            Developer
                        </div> -->
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <!-- <div class="profile-userbuttons">
                        <a href="<?=base_url('produk')?>" class="btn btn-success btn-sm">Pesan</a>
                        <a href="<?=base_url('member/logout')?>" class="btn btn-danger btn-sm">Keluar</a>
                    </div> -->
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="<?php if ($aktif == 'Profil') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil') ?>">
                                <i class="glyphicon glyphicon-home"></i>
                                Overview </a>
                            </li>
                            <li class="<?php if ($aktif == 'Pengaturan Profil') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil/pengaturan') ?>">
                                <i class="glyphicon glyphicon-user"></i>
                                Profil </a>
                            </li>
                            <li class="<?php if ($aktif == 'Pesanan Saya') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil/pesanan') ?>">
                                <i class="glyphicon glyphicon-flag"></i>
                                Pesanan Saya </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>
            <div class="col-md-9">
                <div class="shadow1">
                    <div class="card-body">
                        <div class="profile-content">
							<div class="row">
								<div class="col-md-12">
									<?php if($this->session->flashdata('sukses')){ ?>
									<div role="alert" class="alert alert-warning">
		                            	<i class="fa fa-bell-o"></i> Mohon segera lakukan <strong>Pembayaran</strong> pada Pesanan Anda. 
		                            </div>
									<?php } ?>
								</div>
								<div class="col-md-12">
									<div class="row">
										<div class="col-xl-6 col-md-6" style="margin-bottom: 5%;">
									        <div class="card" style="height: 130px">
									            <div class="card-header border-0 p-2" style="background: rgb(230,0,20);background: linear-gradient(90deg, rgba(230,0,20,1) 0%, rgba(255,39,20,1) 50%, rgba(230,0,20,1) 100%);">
									                <h6 class="m-0 arvo text-white text-center" style="font-size:15px">Pesanan Perlu Dibayar </h6>
									            </div>
									            <div class="card-body py-4 my-2 text-center">
									                <h4 class="font-weight-bold mb-0 my-auto d-block">
									                    19                </h4>
									                 <small class="text-secondary mb-0">Pesanan</small>
									            </div>
									        </div>
									    </div>
										<div class="col-xl-6 col-md-6" style="margin-bottom: 5%;">
									        <div class="card rounded text-center" style="height: 130px">
									            <div class="card-header bg-success border-0 p-2" style="background: linear-gradient(90deg, #f79011 0%, #ffc215 50%, #f79011 100%);">
									                <h6 class="m-0 arvo text-white text-center" style="font-size:15px">Pesanan Diproses</h6>
									            </div>
									            <div class="card-body py-4 my-2 text-center">
									                <h4 class="font-weight-bold mb-0 my-auto d-block">
									                    19                </h4>
									                 <small class="text-secondary mb-0">Pesanan</small>
									            </div>
									        </div>
									    </div>
										<div class="col-xl-6 col-md-6" style="margin-bottom: 5%;">
									        <div class="card" style="height: 130px">
									            <div class="card-header border-0 p-2" style="background: rgb(230,0,20);background:  linear-gradient(90deg, #56a43e 0%, #67cb48 50%, #56a43e 100%);">
									                <h6 class="m-0 arvo text-white text-center" style="font-size:15px">Pesanan Dikirim </h6>
									            </div>
									            <div class="card-body py-4 my-2 text-center">
									                <h4 class="font-weight-bold mb-0 my-auto d-block">
									                    19                </h4>
									                 <small class="text-secondary mb-0">Pesanan</small>
									            </div>
									        </div>
									    </div>
										<div class="col-xl-6 col-md-6" style="margin-bottom: 5%;">
									        <div class="card rounded text-center" style="height: 130px">
									            <div class="card-header bg-success border-0 p-2" style="background:linear-gradient(90deg, rgb(87, 122, 222) 0%, rgb(88, 145, 251) 50%, rgba(87, 122, 222,1) 100%);">
									                <h6 class="m-0 arvo text-white text-center" style="font-size:15px">Pesanan Selesai</h6>
									            </div>
									            <div class="card-body py-4 my-2 text-center">
									                <h4 class="font-weight-bold mb-0 my-auto d-block">
									                    19                </h4>
									                 <small class="text-secondary mb-0">Pesanan</small>
									            </div>
									        </div>
									    </div>
									</div>
							    </div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
				