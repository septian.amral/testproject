<div class="breadcrumb-area pt-35 pb-35 bg-gray-3">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="">Pesanan Saya</a>
                </li>
                <li class="active">Informasi Pembayaran</li>
            </ul>
        </div>
    </div>
</div>
<div class="login-register-area pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 ml-auto mr-auto">
                <div class="login-register-wrapper">
                    <div class="tab-content">
                        <div id="lg1" class="tab-pane active">
                            <div class="login-form-container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 style="margin-top: 5%;"> Metode Pembayaran </h3>
                                        <hr>
                                        <h4>
                                            Bank Transfer Payment
                                        </h4>
                                        <b>
                                            Bank BRI
                                            <div class="norek">
                                                1488 - 0100 - 1007 - 501
                                            </div>
                                            An. Achadina Z
                                        </b>
                                    </div>
                                    <div class="col-md-6" style="text-align: right;">
                                        <img alt="Logo Percetakan Ahza" src="<?=base_url('flone');?>/assets/images/atm.png" style="width: 60%;margin-top: -7%;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function cari_kota() {
        // alert($('#provinsi').val());
        $.ajax({
            url:"<?=base_url('pengiriman/kota');?>",
            type: 'post',
            data:{
                provinsi: $('#provinsi').val(),
            },
            success: function( data, textStatus, jQxhr ){
                var val = JSON.parse(data);
                var len = val.length;
                $("#kota").empty();
                for(var i = 0; i<len; i++){
                    var id = val[i]['city_id'];
                    var name = val[i]['city_name'];
                    var kodepos = val[i]['postal_code'];
                    console.log(val[i]);
                    $("#kota").append("<option value='"+id+"'>"+name+"</option>");
                    // $("#kodepos").append("<input class='form-control' name='kode_pos' id='kode_pos' placeholder='Kode Pos' type='text' value='"+kodepos+"''>");
                }
            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });
        return false;
    };
</script>