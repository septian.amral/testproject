<div class="profil-area pt-95 pb-100">
    <div class="container">
    	<div class="row profile">
            <div class="col-md-3">
                <div class="profile-sidebar">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic" style="text-align: center;">
                        <img src="<?= base_url() ?>images/u1.png" class="img-responsive" alt="">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            <?=$this->session->userdata('username')?>
                        </div>
                        <!-- <div class="profile-usertitle-job">
                            Developer
                        </div> -->
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                     <!-- <div class="profile-userbuttons">
                        <a href="<?=base_url('produk')?>" class="btn btn-success btn-sm">Pesan</a>
                        <a href="<?=base_url('member/logout')?>" class="btn btn-danger btn-sm">Keluar</a>
                    </div> -->
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="<?php if ($aktif == 'Profil') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil') ?>">
                                <i class="glyphicon glyphicon-home"></i>
                                Overview </a>
                            </li>
                            <li class="<?php if ($aktif == 'Pengaturan Profil') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil/pengaturan') ?>">
                                <i class="glyphicon glyphicon-user"></i>
                                Profil </a>
                            </li>
                            <li class="<?php if ($aktif == 'Pesanan Saya') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil/pesanan') ?>">
                                <i class="glyphicon glyphicon-flag"></i>
                                Pesanan Saya </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>
            <div class="col-md-9">
                <div class="shadow1">
                    <div class="card-body">
                        <div class="profile-content">
							<?php echo validation_errors(); ?>
							<?=form_open('Profil/tambah_datadiri');?>	
								<div class="row">
									<div class="col-md-6">
										<legend>Data Akun</legend>
									</div>
									<div class="col-md-6 text-right">
							            <button type="button" class="btn btn-success" data-toggle="modal" href="#notifberhasil"> Edit</button>
									</div>
								</div>
									<div class="row">
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Username</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="username" id="username" placeholder="Username" type="text">
									            </div>
									        </div>
										</div>
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Password</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="password" id="password" placeholder="Password" type="text">
									            </div>
									        </div>
										</div>
									</div>
								<div class="row">
									<div class="col-md-12">
										<legend>Data Pribadi</legend>
									</div>
								</div>
									<div class="form-group">
							            <label for="" class="col-xl-12 col-md-12 form-control-label">Nama Lengkap</label>
							            <div class="col-xl-12 col-md-12">
							                <input class="form-control" name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap" type="text">
							            </div>
							        </div>
									<div class="row">
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Nomor Telepon </label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="no_tlp" id="no_tlp" placeholder="Nomor Telepon/ HP" type="text">
									            </div>
									        </div>
										</div>
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Alamat Email</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="email" id="email" placeholder="Alamat Email" type="text">
									            </div>
									        </div>
										</div>
									</div>
									<div class="form-group">
							            <label for="" class="col-xl-12 col-md-12 form-control-label">Foto Profil</label>
							            <div class="col-xl-12 col-md-12">
							                <input class="form-control" name="gambar" id="gambar" placeholder="Foto Profil" type="file">
							            </div>
							        </div>
								<legend>Alamat Rumah</legend>  
									<div class="form-group">
							            <label for="" class="col-xl-12 col-md-12 form-control-label">Alamat Rumah</label>
							            <div class="col-xl-12 col-md-12">
							                <textarea class="form-control" rows="3" name="alamat" id="alamat"></textarea> 
							            </div>
							        </div>
									<div class="row">
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Provinsi</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="provinsi" id="provinsi" placeholder="Provinsi" type="text">
									            </div>
									        </div>
										</div>
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Kota</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="kota" id="kota" placeholder="Kota" type="text">
									            </div>
									        </div>
										</div>
									</div>
									<div class="row">
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Kelurahan</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="kelurahan" id="kelurhan" placeholder="Kelurahan" type="text">
									            </div>
									        </div>
										</div>
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Kecamatan</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="kecamatan" id="kecamatan" placeholder="Kecamatan" type="text">
									            </div>
									        </div>
										</div>
									</div>
									<div class="row">
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Kode Pos</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="kode_pos" id="kode_pos" placeholder="Kode Pos" type="text">
									            </div>
									        </div>
										</div>
									</div>

								<legend>Alamat Pengiriman</legend>  
									<div class="form-group">
							            <label for="" class="col-xl-12 col-md-12 form-control-label">Alamat Pengiriman</label>
							            <div class="col-xl-12 col-md-12">
							                <textarea class="form-control" rows="3" name="palamat" id="palamat"></textarea> 
							            </div>
							        </div>
									<div class="row">
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Provinsi</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="pprovinsi" id="pprovinsi" placeholder="Provinsi" type="text">
									            </div>
									        </div>
										</div>
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Kota</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="pkota" id="pkota" placeholder="Kota" type="text">
									            </div>
									        </div>
										</div>
									</div>
									<div class="row">
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Kelurahan</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="pkelurahan" id="pkelurhan" placeholder="Kelurahan" type="text">
									            </div>
									        </div>
										</div>
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Kecamatan</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="pkecamatan" id="pkecamatan" placeholder="Kecamatan" type="text">
									            </div>
									        </div>
										</div>
									</div>
									<div class="row">
										<div class="col-xl-6 col-md-6">
											<div class="form-group">
									            <label for="" class="col-xl-12 col-md-12 form-control-label">Kode Pos</label>
									            <div class="col-xl-12 col-md-12">
									                <input class="form-control" name="pkode_pos" id="pkode_pos" placeholder="Kode Pos" type="text">
									            </div>
									        </div>
										</div>
									</div>

							        <div class="pull-right">
							            <button type="reset" class="btn btn-danger"> Batal</button>
							            <button type="submit" class="btn btn-primary"> Simpan</button>
							        </div>
							</form>
							<div class="modal fade" id="notifberhasil" role="dialog">
							    <div class="modal-dialog">
							        <div class="modal-content">
							            <div class="modal-header">
							                <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
							             </div>
										
							            <div class="modal-body">
							               
											<div class="thank-you-pop">
												<div class="swal2-icon swal2-success swal2-animate-success-icon" style="display: flex;">
												 	<div class="swal2-success-circular-line-left" style="background-color: rgb(255, 255, 255);"></div>
												   	<span class="swal2-success-line-tip"></span>
												   	<span class="swal2-success-line-long"></span>
												   	<div class="swal2-success-ring"></div> 
												   	<div class="swal2-success-fix" style="background-color: rgb(255, 255, 255);"></div>
												   	<div class="swal2-success-circular-line-right" style="background-color: rgb(255, 255, 255);"></div>
												</div>
												<h1>Berhasil!</h1>
												<p>Data Anda Berhasil Terupdate</p>
												<h3 class="cupon-pop">Your Id: <span>12345</span></h3>
												
												</div>
							                 
							            </div>
										
							        </div>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
