<div class="profil-area pt-95 pb-100">
    <div class="container">
        
        <div class="row profile">
            <div class="col-md-3">
                <div class="profile-sidebar">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic" style="text-align: center;">
                        <img src="<?= base_url() ?>images/u1.png" class="img-responsive" alt="">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                             <?=$this->session->userdata('username')?>
                        </div>
                        <!-- <div class="profile-usertitle-job">
                            Developer
                        </div> -->
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <!-- <div class="profile-userbuttons">
                        <a href="<?=base_url('produk')?>" class="btn btn-success btn-sm">Pesan</a>
                        <a href="<?=base_url('member/logout')?>" class="btn btn-danger btn-sm">Keluar</a>
                    </div> -->
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="<?php if ($aktif == 'Profil') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil') ?>">
                                <i class="glyphicon glyphicon-home"></i>
                                Overview </a>
                            </li>
                            <li class="<?php if ($aktif == 'Pengaturan Profil') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil/pengaturan') ?>">
                                <i class="glyphicon glyphicon-user"></i>
                                Profil </a>
                            </li>
                            <li class="<?php if ($aktif == 'Pesanan Saya') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil/pesanan') ?>">
                                <i class="glyphicon glyphicon-flag"></i>
                                Pesanan Saya </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>
            <div class="col-md-9">
                <div class="page">
                    <!-- <div class="search-section">
                        <div class="container">
                            <h1><?=$judul?></h1>
                        </div>
                    </div> -->
                    <div class="breadcrumbs">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>
                                        <?=$aktif?>
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <ol style="text-align: right;">
                                <li><a href="/"><?=$judul?></a>
                                </li>
                                <li class="active"><?=$aktif?></li>
                            </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shadow1">
                    <div class="card-body">
                        <div class="profile-content">
                            <div class="single-product-detail">
                                <div class="row">
                                    <div class="col-12 col-lg-12">
                                        <div class="demo">
                                            <ul id="lightSliderGallery" class="lightSlider" data-gallery="true" data-item="1"
                                                data-loop="true" data-auto="false" data-thumbs="4" data-controls="false"
                                                data-position="middle">
                                                <li data-thumb="<?= base_url() ?>images/slide-undangan.jpg">
                                                    <img src="<?= base_url() ?>images/slide-undangan.jpg">
                                                </li>
                                                <li data-thumb="assets/img/demo/ss2.png">
                                                    <img src="assets/img/demo/ss2.png">
                                                </li>
                                                <li data-thumb="assets/img/demo/ss3.png">
                                                    <img src="assets/img/demo/ss3.png">
                                                </li>
                                                <li data-thumb="assets/img/demo/ss4.png">
                                                    <img src="assets/img/demo/ss4.png">
                                                </li>
                                                <li data-thumb="assets/img/demo/ss5.png">
                                                    <img src="assets/img/demo/ss5.png">
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>