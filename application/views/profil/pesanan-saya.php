<div class="profil-area pt-95 pb-100">
    <div class="container">
        <div class="row profile">
            <div class="col-md-3">
                <div class="profile-sidebar">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic" style="text-align: center;">
                        <img src="<?= base_url() ?>images/u1.png" class="img-responsive" alt="">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                           <?=$this->session->userdata('username')?>
                       </div>
                        <!-- <div class="profile-usertitle-job">
                            Developer
                        </div> -->
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <!-- <div class="profile-userbuttons">
                        <a href="<?=base_url('produk')?>" class="btn btn-success btn-sm">Pesan</a>
                        <a href="<?=base_url('member/logout')?>" class="btn btn-danger btn-sm">Keluar</a>
                    </div> -->
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="<?php if ($aktif == 'Profil') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil') ?>">
                                    <i class="glyphicon glyphicon-home"></i>
                                Overview </a>
                            </li>
                            <li class="<?php if ($aktif == 'Pengaturan Profil') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil/pengaturan') ?>">
                                <i class="glyphicon glyphicon-user"></i>
                                Profil </a>
                            </li>
                            <li class="<?php if ($aktif == 'Pesanan Saya') { echo 'active'; } ?>">
                                <a href="<?= site_url('profil/pesanan') ?>">
                                    <i class="glyphicon glyphicon-flag"></i>
                                Pesanan Saya </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>
            <div class="col-md-9">
                <div class="shadow1">
                    <div class="card-body">
                        <div class="profile-content">
                            <?= $pagination ?>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                No
                                            </th>
                                            <th class="text-center">
                                                Tgl Pemesanan
                                            </th>
                                            <th class="text-center">
                                                No Pemesanan
                                            </th>
                                            <th class="text-center">
                                                Jumlah Barang (Pcs)
                                            </th>
                                            <th class="text-center">
                                                Nominal
                                            </th>
                                            <th class="text-center">
                                                Status
                                            </th>
                                            <th class="text-center">
                                                Bukti Transfer
                                            </th>
                                            <th class="text-center">
                                                Aksi
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach($list_invoice as $items): ?>
                                        <tr>
                                            
                                                <td class="text-center">
                                                    <?= ($offset + $no++) ?>
                                                </td>
                                                <td>
                                                    <?=$items['created_date'] ?>
                                                </td>
                                                <td>
                                                    <?= $items['invoice_no'] ?>
                                                </td>
                                                <td>
                                                    <?=$items['jumlah'] ?>
                                                </td>
                                                <td>
                                                    <?=$items['totalbayar']?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if ($items['status'] == 1)
                                                            echo 'Belum Melakukan Pembayaran';
                                                        else 
                                                            echo 'Pembayaran sudah dikonfirmasi';
                                                    ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php if (isset($items['buktipem'])) { ?>
                                                        <a  target="_blank" href="<?= base_url().UPLOAD_IMAGE_LOCATION.$items['buktipem']?>">
                                                            <i class="fa fa-file-image-o" aria-hidden="true" style="font-size: 20px"></i>
                                                        </a>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center">
                                                    <a href="<?= base_url() ?>profil/detail_pesanan/<?= $items['id'] ?>">Detail </a>
                                                    <br>
                                                    <?php if ($items['status'] == 1) { ?>
                                                        <a data-toggle="modal" onclick="bind_table(this)" data-target="#upload_bukti" href="">Upload Bukti Transfer</a>
                                                    <?php } ?>
                                                </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?= $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="upload_bukti">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open_multipart('profil/upload_bukti_transfer');?>
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Upload Bukti Transfer</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
            <input type="hidden" id="id_pesanan" name="id_pesanan" value="" />
                <input type="file" name="bukti_transfer" />

                <br /><br/>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <!--
                <a onclick="peringtan()" type="button" class="btn btn-success" data-dismiss="modal">Oke</a>
                <a type="button" class="btn btn-danger" data-dismiss="modal">Batal</a>
                -->
                <input type="submit" class="btn btn-success" value="upload" />
            </div>
            </form>
        </div>
    </div>
</div>

<script src="<?=base_url('assets/vendor/jquery/jquery.min.js');?>"></script>
<script type="text/javascript">
    function bind_table(elem) {
        var id_cart = $(elem).parent().parent().find('td').first().text().trim();
        var temp = $(elem).parent().parent().find('td').first().next().next().text().trim();
        var res = temp.split("-");
        var id_pesanan = parseInt(res[3]);

        $('#id_pesanan').val(id_pesanan);
    }    
</script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>

