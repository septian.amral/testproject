<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');
    }

    public function index()
    {
        cek_login();
        $this->data_admin();
    }

    public function data_admin()
    {
        cek_login();
        $data = [
            'judul'      => 'Data Pengguna',
            'sub_judul'  => 'Data Pengguna' . nama_toko,
            'data_admin' => $this->admin_model->data_admin(),
        ];
        $this->load->view('template/admin/header', $data);
        $this->load->view('template/admin/navbar', $data);
        $this->load->view('admin/data_admin', $data);
        $this->load->view('template/admin/footer', $data);
    }

    public function tambah_admin()
    {
        cek_login();

        $data = [
            'judul'     => 'Tambah Pengguna',
            'sub_judul' => 'Tambah Data Pengguna' . nama_toko,
        ];

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('level', 'Level', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('template/admin/header', $data);
            $this->load->view('template/admin/navbar', $data);
            $this->load->view('admin/tambah_admin', $data);
            $this->load->view('template/admin/footer', $data);
        } else {
            $this->session->set_flashdata('sukses', 'Berhasil Tambah Admin');
            $this->admin_model->tambah_admin();
            redirect('admin', 'refresh');
        }
    }

    public function ubah_admin($id)
    {
        cek_login();

        $data = [
            'judul'      => 'Ubah Data Pengguna',
            'sub_judul'  => 'Ubah Data Pengguna' . nama_toko,
            'data_admin' => $this->admin_model->ubah_admin($id),
        ];

        if ($this->form_validation->run() === false) {
            $this->load->view('template/admin/header', $data);
            $this->load->view('template/admin/navbar', $data);
            $this->load->view('admin/ubah_admin', $data);
            $this->load->view('template/admin/footer', $data);
        } else {
            $this->session->set_flashdata('sukses', 'Berhasil Ubah Admin');
            $this->admin_model->tambah_admin();
            redirect('admin', 'refresh');
        }
    }

    public function hapus_admin($id, $status) {
        if ($status == 1) {
            $hapus = $this->admin_model->hapus_admin($id, 0);
            if ($hapus) {
                $this->session->set_flashdata('sukses', 'Berhasil Nonaktifkan Admin');
                redirect('admin', 'refresh');
            } else {
                $this->session->set_flashdata('gagal', 'Gagal Hapus Admin');
                redirect('admin', 'refresh');
            }
        }else{
            $hapus = $this->admin_model->hapus_admin($id, 1);
            if ($hapus) {
                $this->session->set_flashdata('sukses', 'Berhasil Aktifkan Admin');
                redirect('admin', 'refresh');
            } else {
                $this->session->set_flashdata('gagal', 'Gagal Hapus Admin');
                redirect('admin', 'refresh');
            }
        }
    }


    public function simpan_ubah()
    {
        cek_login();

        $data = [
            'judul'     => 'Update Pengguna',
            'sub_judul' => 'Update Pengguna' . nama_toko,
        ];

        $this->form_validation->set_rules('id_pengguna', 'ID Missing', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('level', 'Level', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('template/admin/header', $data);
            $this->load->view('template/admin/navbar', $data);
            $this->load->view('admin/tambah_admin', $data);
            $this->load->view('template/admin/footer', $data);
        } else {
            $this->session->set_flashdata('sukses', 'Berhasil Ubah Admin');
            $this->admin_model->simpan_ubah();
            redirect('admin', 'refresh');
        }
    }

    public function login()
    {
        $data = [
            'judul' => 'Login -' . nama_toko,
        ];
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[12]');

        if ($this->form_validation->run() == false) {
            $this->load->view('template/admin/header_login', $data);
            $this->load->view('admin/login', $data);
            $this->load->view('template/admin/footer_login', $data);
        } else {
            $cek = $this->admin_model->cek_user_pass();
            if ($cek) {
                $userdata = [
                    'username'  => $cek->username,
                    'name'      => $cek->nama,
                    'logged_in' => true,
                    'level'     => 'admin'
                ];

                $this->session->set_userdata($userdata);
                $this->session->set_flashdata('pesan', 'Sukses Masuk');
                redirect('admin', 'refresh');
            } else {
                $this->session->set_flashdata('pesan', 'Gagal Masuk');
                redirect('admin/login', 'refresh');
            }
        }
    }

    public function logout()
    {
        $userdata = ['username', 'name'];
        $this->session->unset_userdata($userdata);
        $this->session->set_flashdata('pesan', 'Sukses Keluar');
        redirect('admin/login', 'refresh');
    }
}
