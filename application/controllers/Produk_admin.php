<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk_admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['produk_model', 'kategori_model']);
    }

    public function index($id = null)
    {
        $this->data_produk();
    }

    public function data_produk()
    {

        $data = [
            'judul'       => 'Data Produk',
            'sub_judul'   => 'Data Produk ' . nama_toko,
            'data_produk' => $this->produk_model->data_produk(),

        ];
        $this->load->view('template/admin/header', $data);
        $this->load->view('template/admin/navbar', $data);
        $this->load->view('admin/data_produk', $data);
        $this->load->view('template/admin/footer', $data);
    }

    public function tambah_produk()
    {
        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');

        $data = [
            'judul'         => 'Tambah Produk',
            'sub_judul'     => 'Tambah Data Produk ' . nama_toko,
            'data_kategori' => $this->kategori_model->data_kategori(),

        ];

        $this->form_validation->set_rules('namaproduk', 'Nama Produk', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        $this->form_validation->set_rules('id_kategori', 'ID Kategori', 'required');
        $this->form_validation->set_rules('harga', 'ID Kategori', 'required');
        $this->form_validation->set_rules('berat', 'Berat', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('template/admin/header', $data);
            $this->load->view('template/admin/navbar', $data);
            $this->load->view('admin/tambah_produk', $data);
            $this->load->view('template/admin/footer', $data);
        } else {
            $dataInfo = [];
            $files    = $_FILES;
            $cpt      = count($_FILES['userfile']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['userfile']['name']     = $files['userfile']['name'][$i];
                $_FILES['userfile']['type']     = $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']    = $files['userfile']['error'][$i];
                $_FILES['userfile']['size']     = $files['userfile']['size'][$i];

                $config['upload_path']   = './gambar/';
                $config['file_name']     = 'ahza' . date('dmy');
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']      = '2048000';
                $config['max_width']     = '1024';
                $config['max_height']    = '768';

                $this->load->library('upload', $config);
                $this->upload->do_upload();
                $dataInfo[] = $this->upload->data();
            }
            $gbrprod = [
                'gambar'  => $dataInfo[0]['file_name'],
                'gambar1' => $dataInfo[1]['file_name'],
                'gambar2' => $dataInfo[2]['file_name'],
            ];
            if (!$this->upload->do_upload()) {
                $this->session->set_flashdata('gagal', $this->upload->display_errors());
                redirect('produk_admin/tambah_produk', 'refresh');
            }

            $this->produk_model->tambah_produk($gbrprod);
            $this->session->set_flashdata('sukses', "Berhasil Tambah Produk");
            redirect('produk_admin', 'refresh');
        }
    }

    public function ubah_produk($id_produk)
    {

        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');

        $data = [
            'judul'         => 'Tambah Produk',
            'sub_judul'     => 'Tambah Data Produk ' . nama_toko,
            'data_produk'   => $this->produk_model->ubah_produk($id_produk),
            'data_kategori' => $this->kategori_model->data_kategori(),
        ];

        // print_r('<pre>');
        // print_r($data);
        if ($this->form_validation->run() === false) {
            $this->load->view('template/admin/header', $data);
            $this->load->view('template/admin/navbar', $data);
            $this->load->view('admin/ubah_produk', $data);
            $this->load->view('template/admin/footer', $data);
        } else {
            $this->produk_model->tambah_produk();
            redirect('produk_admin', 'refresh');
        }
    }

    public function hapus_produk($id_produk)
    {
        $hapus = $this->produk_model->hapus_produk($id_produk);
        if ($hapus) {
            $this->session->set_flashdata('sukses', 'Berhasil menghapus produk');
            redirect('produk_admin', 'refresh');
        } else {
            $this->session->set_flashdata('gagal', 'Gagal menghapus produk');
            redirect('produk_admin', 'refresh');
        }
    }

    public function simpan_ubah_produk()
    {
        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');

        $data = [
            'judul'         => 'Tambah Produk',
            'sub_judul'     => 'Tambah Data Produk ' . nama_toko,
            'data_produk'   => $this->produk_model->ubah_produk($this->input->post('id_produk')),
            'data_kategori' => $this->kategori_model->data_kategori(),
        ];

        $this->form_validation->set_rules('namaproduk', 'Nama Produk', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        $this->form_validation->set_rules('id_kategori', 'ID Kategori', 'required');
        $this->form_validation->set_rules('harga', 'ID Kategori', 'required');
        $this->form_validation->set_rules('berat', 'Berat', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('template/admin/header', $data);
            $this->load->view('template/admin/navbar', $data);
            $this->load->view('admin/ubah_produk', $data);
            $this->load->view('template/admin/footer', $data);
        } else {
            $dataInfo = [];
            $files    = $_FILES;
            $cpt      = count($_FILES['userfile']['name']);
            if ($cpt < 3) {

                $this->session->set_flashdata('gagal','Minimum dan Maksimum Gambar Harus 3');
                redirect('produk_admin/tambah_produk', 'refresh');
               
            }
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['userfile']['name']     = $files['userfile']['name'][$i];
                $_FILES['userfile']['type']     = $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']    = $files['userfile']['error'][$i];
                $_FILES['userfile']['size']     = $files['userfile']['size'][$i];

                $config['upload_path']   = './gambar/';
                $config['file_name']     = 'ahza' . date('dmy');
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']      = '2048000';
                $config['max_width']     = '1024';
                $config['max_height']    = '768';

                $this->load->library('upload', $config);
                $this->upload->do_upload();
                $dataInfo[] = $this->upload->data();
            }
            $gbrprod = [
                'gambar'  => $dataInfo[0]['file_name'],
                'gambar1' => $dataInfo[1]['file_name'],
                'gambar2' => $dataInfo[2]['file_name'],
            ];
            if (!$this->upload->do_upload()) {
                $this->session->set_flashdata('gagal', $this->upload->display_errors());
                redirect('produk_admin/tambah_produk', 'refresh');
            }

            $this->produk_model->simpan_ubah($gbrprod);
            $this->session->set_flashdata('sukses', "Berhasil Ubah Produk");
            redirect('produk_admin', 'refresh');
        }
    }
}

/* End of file Produk_admin.php */
/* Location: ./application/controllers/Produk_admin.php */
