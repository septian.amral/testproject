<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['member_model', 'kategori_model', 'produk_model']);
    }

    public function index()
    {
        $this->selamat_datang();
    }

    public function data_member()
    {

        $data = [
            'judul'       => 'Data Member',
            'sub_judul'   => 'Data Member ' . nama_toko,
            'data_member' => $this->member_model->data_member(),
        ];
        $this->load->view('template/admin/header', $data);
        $this->load->view('template/admin/navbar', $data);
        $this->load->view('admin/data_member', $data);
        $this->load->view('template/admin/footer', $data);
    }

    public function detail_member()
    {
        $data = [
            'judul'       => 'Detail Member',
            'sub_judul'   => 'Detail Member ' . nama_toko,
            'data_member' => $this->member_model->detail($id),
        ];
        $this->load->view('template/admin/header', $data);
        $this->load->view('template/admin/navbar', $data);
        $this->load->view('admin/detail_member', $data);
        $this->load->view('template/admin/footer', $data);
    }

    public function provinsi()
    {
        echo cek_provinsi();
    }

    public function buat_member()
    {
        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');

        $data = [
            'judul'         => 'Buat Member',
            'aktif'         => 'beranda',
            'sub_judul'     => 'Buat Data Member ' . nama_toko,
        ];

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('no_tlp', 'Nomor Telepon', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('gambar', 'Gambar', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
        $this->form_validation->set_rules('kota', 'Kota', 'required');
        $this->form_validation->set_rules('kelurahan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
        $this->form_validation->set_rules('kode_pos', 'Kode Pos', 'required');
        $this->form_validation->set_rules('palamat', 'Alamat Pengiriman', 'required');
        $this->form_validation->set_rules('pprovinsi', 'Provinsi Pengiriman', 'required');
        $this->form_validation->set_rules('pkota', 'Kota Pengiriman', 'required');
        $this->form_validation->set_rules('pkelurahan', 'Kelurahan Pengiriman', 'required');
        $this->form_validation->set_rules('pkecamatan', 'Kecamatan Pengiriman', 'required');
        $this->form_validation->set_rules('pkode_pos', 'Kode Pos Pengiriman', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('template/member/header', $data);
            $this->load->view('template/member/navbar', $data);
            $this->load->view('member/daftar', $data);
            $this->load->view('template/member/footer', $data);
        } else {
            $this->member_model->tambah_member();
            $this->session->set_flashdata('sukses', 'Selamat Anda Berhasil Mendaftar');
            redirect('profil', 'refresh');
        }
    }

    public function ubah_member($id)
    {

        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');

        $data = [
            'judul'       => 'Tambah Member',
            'sub_judul'   => 'Tambah Data Member ' . nama_toko,
            'data_member' => $this->member_model->ubah_member($id),
        ];

        if ($this->form_validation->run() === false) {
            $this->load->view('template/member/header', $data);
            $this->load->view('template/member/navbar', $data);
            $this->load->view('member/ubah_member', $data);
            $this->load->view('template/member/footer', $data);
        } else {
            $this->member_model->tambah_member();

            $this->lihat_member();
        }
    }

    public function hapus_member($id)
    {
        $hapus = $this->member_model->hapus_member($id);
        if ($hapus) {
            $this->lihat_member();
        } else {
            $this->lihat_member();
        }
    }

    public function masuk()
    {
        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');

        $data = [
            'aktif'         => 'beranda',
            'judul'         => 'Buat Member',
            'sub_judul'     => 'Buat Data Member ' . nama_toko,
            // 'data_produk'      => $this->produk_model->data_produk(),
            'data_kategori' => $this->kategori_model->data_kategori(),
        ];

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('template/member/header', $data);
            $this->load->view('template/member/navbar', $data);
            $this->load->view('member/login', $data);
            $this->load->view('template/member/footer', $data);
        } else {
            $cek = $this->member_model->cek_masuk();
            if ($cek) {
                $userdata = [
                    'id_pelanggan' => $cek->id_pelanggan,
                    'username'     => $cek->username,
                    'logged_in'    => true,
                    'level'        => 'member',
                ];
                $this->session->set_userdata($userdata);
                redirect('profil', 'refresh');
            } else {
                $this->session->set_flashdata('pesan', 'Gagal masuk. Username atau Password Salah');
                redirect('member/masuk', 'refresh');
            }
        }
    }

    public function selamat_datang()
    {
        $data = [
            'judul'         => 'Ahza',
            'aktif'         => 'beranda',
            'sub_judul'     => 'Sub Judulnya apa ' . nama_toko,
            'data_produk'   => $this->produk_model->data_produk(),
            'data_kategori' => $this->kategori_model->data_kategori(),
        ];

        $this->load->view('template/member/header', $data);
        $this->load->view('template/member/navbar', $data);
        $this->load->view('member/beranda', $data);
        $this->load->view('template/member/footer');
    }

    public function akunku()
    {
        $data = [
            'judul'         => 'Ahza',
            'aktif'         => 'beranda',
            'sub_judul'     => 'Sub Judulnya apa ' . nama_toko,
            'data_produk'   => $this->produk_model->data_produk(),
            'data_kategori' => $this->kategori_model->data_kategori(),
        ];

        $this->load->view('template/member/header', $data);
        $this->load->view('template/member/navbar', $data);
        $this->load->view('member/akunku', $data);
        $this->load->view('template/member/footer');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->unset_userdata('username');
        $this->session->set_flashdata('pesan', 'Sukses Keluar');
        redirect('member/daftar', 'refresh');
    }
}
