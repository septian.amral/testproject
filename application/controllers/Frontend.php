<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(['admin_model', 'kategori_model', 'produk_model']);
    }

    public function index() {
        $this->beranda();
    }

    public function beranda() {
        $data = [
            'judul'            => 'Ahza',
            'sub_judul'        => 'Sub Judulnya apa ' . nama_toko,
            'aktif'            => 'Beranda',
            // 'data_produk'      => $this->produk_model->data_produk(),
            // 'data_kategori'    => $this->kategori_model->data_kategori(),
        ];

        $this->load->view('template/member/header', $data);
        $this->load->view('template/member/navbar', $data);
        $this->load->view('member/beranda', $data);
        $this->load->view('template/member/footer');
    }

    public function produk() {
        $data = [
            'judul'            => 'Ahza',
            'sub_judul'        => 'Sub Judulnya apa ' . nama_toko,
            'aktif'            => 'Produk',
            'data_produk'      => $this->produk_model->data_produk(),
            'data_kategori'    => $this->kategori_model->data_kategori(),
        ];

        $this->load->view('template/member/header', $data);
        $this->load->view('template/member/navbar', $data);
        $this->load->view('member/produk', $data);
        $this->load->view('template/member/footer');
    }

    public function tentang_kami() {
        $data = [
            'judul'            => 'Ahza',
            'sub_judul'        => 'Sub Judulnya apa ' . nama_toko,
            'aktif'            => 'Tentang Ahza',
            // 'data_produk'      => $this->produk_model->data_produk(),
            // 'data_kategori'    => $this->kategori_model->data_kategori(),
        ];

        $this->load->view('template/member/header', $data);
        $this->load->view('template/member/navbar', $data);
        $this->load->view('member/tentang', $data);
        $this->load->view('template/member/footer');
    }

    public function kontak() {
        $data = [
            'judul'            => 'Ahza',
            'sub_judul'        => 'Sub Judulnya apa ' . nama_toko,
            'aktif'            => 'Hubungi Kami',
            // 'data_produk'      => $this->produk_model->data_produk(),
            // 'data_kategori'    => $this->kategori_model->data_kategori(),
        ];

        $this->load->view('template/member/header', $data);
        $this->load->view('template/member/navbar', $data);
        $this->load->view('member/kontak', $data);
        $this->load->view('template/member/footer');
    }
}
