<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('kategori_model');
    }
    public function index()
    {
        $this->data_kategori();
    }

    public function data_kategori()
    {

        $data = [
            'judul'         => 'Data Kategori',
            'sub_judul'     => 'Data Kategori ' . nama_toko,
            'data_kategori' => $this->kategori_model->data_kategori(),
        ];
        $this->load->view('template/admin/header', $data);
        $this->load->view('template/admin/navbar', $data);
        $this->load->view('admin/data_kategori', $data);
        $this->load->view('template/admin/footer', $data);
    }

    public function tambah_kategori()
    {
        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');

        $data = [
            'judul'     => 'Tambah Kategori',
            'sub_judul' => 'Tambah Data Kategori ' . nama_toko,
        ];

        $this->form_validation->set_rules('namakategori', 'Nama Kategori', 'required');
        // $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('template/admin/header', $data);
            $this->load->view('template/admin/navbar', $data);
            $this->load->view('admin/tambah_kategori', $data);
            $this->load->view('template/admin/footer', $data);
        } else {
            $this->session->set_flashdata('sukses', 'Berhasil Tambah Kategori');
            $this->kategori_model->tambah_kategori();
            redirect('kategori', 'refresh');
        }
    }
    public function ubah_kategori($id)
    {

        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');

        $data = [
            'judul'         => 'Ubah Kategori',
            'sub_judul'     => 'Ubah Data Kategori ' . nama_toko,
            'data_kategori' => $this->kategori_model->ubah_kategori($id),
        ];

        if ($this->form_validation->run() === false) {
            $this->load->view('template/admin/header', $data);
            $this->load->view('template/admin/navbar', $data);
            $this->load->view('admin/ubah_kategori', $data);
            $this->load->view('template/admin/footer', $data);
        } else {
            $this->session->set_flashdata('sukses', 'Berhasil Ubah Kategori');
            $this->kategori_model->tambah_kategori();
            redirect('kategori', 'refresh');
        }

    }
    public function hapus_kategori($id)
    {
        $hapus = $this->kategori_model->hapus_kategori($id);
        if ($hapus) {
            $this->session->set_flashdata('sukses', 'Berhasil Hapus Kategori');
            redirect('kategori', 'refresh');
        } else {
            $this->session->set_flashdata('gagal', 'Gagal Hapus Kategori');
            redirect('kategori', 'refresh');
        }
    }
    public function simpan_ubah_kategori()
    {

        $this->load->helper(['form', 'url']);
        $this->load->library('form_validation');

        $data = [
            'judul'     => 'Tambah Kategori',
            'sub_judul' => 'Tambah Data Kategori ' . nama_toko,
        ];

        $this->form_validation->set_rules('id_kategori', 'ID missing', 'required');
        $this->form_validation->set_rules('namakategori', 'Nama Kategori', 'required');
        // $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('template/admin/header', $data);
            $this->load->view('template/admin/navbar', $data);
            $this->load->view('admin/ubah_kategori', $data);
            $this->load->view('template/admin/footer', $data);
        } else {
            $this->session->set_flashdata('sukses', 'Berhasil Ubah Kategori');
            $this->kategori_model->simpan_ubah();
            redirect('kategori', 'refresh');
        }

    }
}
