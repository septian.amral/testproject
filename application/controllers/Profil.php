<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//$this->load->model('profil_customer_model');
		$this->load->model(['pesanan_model']);
		$this->load->model(['invoice_model']);
		$this->load->library(['cart', 'form_validation','Custom_pagination']);
		cek_login_member();
	}

	public function index()
	{	
		$data['judul']	= 'Profil';
		$data['aktif']	= 'Profil';

		$this->load->view('template/member/header', $data);
		$this->load->view('template/member/navbar', $data);
		$this->load->view('profil/main');
		$this->load->view('template/member/footer');
	}

	public function pengaturan()
	{
		$data['judul']	= 'Profil';
		$data['aktif']	= 'Pengaturan Profil';

		$this->load->view('template/member/header', $data);
		$this->load->view('template/member/navbar', $data);
		$this->load->view('profil/pengaturan');
		$this->load->view('template/member/footer');
	}
	public function tambah_datadiri() {

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('tgl_lahir', 'tgl_lahir', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'jenis_kelamin', 'required');

		if ($this->form_validation->run() == FALSE) {

			$data['judul']	= 'Profil';
			$data['aktif']	= 'Pengaturan Profil';

			$this->load->view('template/member/header', $data);
			$this->load->view('template/member/navbar', $data);
			$this->load->view('profil/pengaturan');
			$this->load->view('template/member/footer');
		} 
		else {
			$this->profil_customer_model->simpan_datadiri();

			//$this->semua_produk();
		}
	}

	public function pesanan()
	{
		$data = [
			'judul' => 'Profil',
			'aktif'=> 'Pesanan Saya',
			//'pilihpem' => $pilihpem

		];

		$use_limit = FALSE;
        $mysql_query = $this->_generate_mysql_query($this->session->userdata('id_pelanggan'), $use_limit);
        $query = $this->_custom_query($mysql_query);
        $total_items = $query->num_rows();

        $use_limit = TRUE;
        $mysql_query = $this->_generate_mysql_query($this->session->userdata('id_pelanggan'), $use_limit);


        $pagination_data['template'] = 'public_bootstrap';
        $pagination_data['target_base_url'] = $this->get_target_pagination_base_url();
        $pagination_data['total_rows'] = $total_items;
        $pagination_data['offset_segment'] = 0;
        $pagination_data['limit'] = $this->get_limit();
        $data['pagination'] = $this->custom_pagination->_generate_pagination($pagination_data);

        $query = $this->_custom_query($mysql_query);

		//$where = array('invoice.id_pelanggan' => $this->session->userdata('id_pelanggan'));
		//$data['list_invoice'] = $this->invoice_model->get($where);
		$query = $this->_custom_query($mysql_query);
		$data['list_invoice'] = $query->result_array();
		$data['offset'] = $this->get_offset();

		$this->load->view('template/member/header', $data);
		$this->load->view('template/member/navbar', $data);
		$this->load->view('profil/pesanan-saya');
		$this->load->view('template/member/footer');
	}

	public function detail_pesanan()
	{
		$invoice_id = $this->uri->segment(3);
		
		$data['judul']	= 'Profil';
		$data['aktif']	= 'Pesanan Saya';

		$where = array('invoice_id' => $invoice_id);
        $data['list_pesanan'] = $this->pesanan_model->get($where);
        $data['invoice'] = $this->invoice_model->get(array('id' => $invoice_id))[0];
        //var_dump($data['invoice']); die();

		$this->load->view('template/member/header', $data);
		$this->load->view('template/member/navbar', $data);
		$this->load->view('profil/pesanan-detail', $data);
		$this->load->view('template/member/footer');
	}

	public function upload_bukti_transfer()
	{
		$id_pesanan = $this->input->post('id_pesanan');
		
		$file_ext = pathinfo($_FILES["bukti_transfer"]["name"], PATHINFO_EXTENSION);

		$config['upload_path']          = './gambar/';
		$config['file_name']     = $this->session->userdata('id_pelanggan'). '-'.date('dmy').'-buktf-'.
                    $id_pesanan;
		$config['allowed_types']        = 'gif|jpg|png|JPG|';
		$config['max_size']             = 1000000;
		//$config['max_width']            = 1024;
		//$config['max_height']           = 768;
	
		$this->load->library('upload', $config);
	
		if ( ! $this->upload->do_upload('bukti_transfer')){
			$error = array('error' => $this->upload->display_errors());
			//$this->load->view('v_upload', $error);
			redirect('profil/pesanan');
		}else{
		    $dataInfo[] = $this->upload->data();
			$data = array('upload_data' => $this->upload->data());
			$data_pesanan['buktipem'] = $dataInfo[0]['file_name'];
			$where = array('id'=>$id_pesanan);
			$this->invoice_model->update($data_pesanan, $where);
			//$this->load->view('v_upload_sukses', $data);
			redirect('profil/pesanan');
		}
	}

	public function info_pembayaran()
	{
		$id_pesanan = $this->uri->segment(3);
		
		$data['judul']	= 'Profil';
		$data['aktif']	= 'Info Pembayaran';

		$this->load->view('template/member/header', $data);
		$this->load->view('template/member/navbar', $data);
		$this->load->view('profil/info-pembayaran', $data);
		$this->load->view('template/member/footer');
	}

	function get_target_pagination_base_url()
    {
    	$first_bit = $this->uri->segment(1);
    	$second_bit = $this->uri->segment(2);
    	//$third_bit = $this->uri->segment(3);
    	//$target_base_url = base_url().$first_bit."/".$second_bit."/".$third_bit;
    	$target_base_url = base_url().$first_bit."/".$second_bit;
    	return $target_base_url;
    }

    function _generate_mysql_query($update_id = "", $use_limit)
    {
    	//NOTE: use_limit can be TRUE or FALSE

    	$mysql_query = "
    	SELECT invoice.*,pelanggan.username
    		FROM invoice
				INNER JOIN pelanggan ON invoice.id_pelanggan = pelanggan.id_pelanggan
			WHERE invoice.id_pelanggan=$update_id
    		ORDER by invoice.status asc, invoice.id desc
    	";

    	if ($use_limit == TRUE) {
    		$limit = $this->get_limit();
    		$offset = $this->get_offset();
    		$mysql_query .= " limit ".$offset.", ".$limit;
    	}

    	return $mysql_query;
    }

    function get_limit()
    {
    	$limit = 10;
    	return $limit;
    }

    function get_offset()
    {
    	$offset = $this->uri->segment(3);
    	if (!is_numeric($offset)) {
    		$offset = 0;
		}

    	return $offset;
    }

    function _custom_query($mysql_query)
    {
        $query = $this->invoice_model->_custom_query($mysql_query);
        return $query;
    }
}
