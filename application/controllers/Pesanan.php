<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Pesanan
 */
class Pesanan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->load->model(['produk_model', 'kategori_model', 'subkategori_model']);
        $this->load->model(['pesanan_model','invoice_model']);
        $this->load->library(['cart', 'form_validation', 'Custom_pagination']);
        $this->load->helper('form');
    }

    public function index()
    {
        // Redirect if the cart is empty
        /*
        if ($this->cart->total_items() <= 0) {
            redirect(base_url(), 'refresh');
        }
        */

        cek_login();

        $data = [
            'judul'     => 'Update Pengguna',
            'sub_judul' => 'Update Pengguna' . nama_toko,
        ];

        $where = '';
        //$data['list_invoice'] = $this->invoice_model->get($where);

        //count the items that belong to this homepage offer
        $use_limit = FALSE;
        $mysql_query = $this->_generate_mysql_query("", $use_limit);
        $query = $this->_custom_query($mysql_query);
        $total_items = $query->num_rows();

        $use_limit = TRUE;
        $mysql_query = $this->_generate_mysql_query("", $use_limit);


        $pagination_data['template'] = 'public_bootstrap';
        $pagination_data['target_base_url'] = $this->get_target_pagination_base_url();
        $pagination_data['total_rows'] = $total_items;
        $pagination_data['offset_segment'] = 0;
        $pagination_data['limit'] = $this->get_limit();
        $data['pagination'] = $this->custom_pagination->_generate_pagination($pagination_data);

        $query = $this->_custom_query($mysql_query);
        $data['list_invoice'] = $query->result_array();
        $data['offset'] = $this->get_offset();
        $this->load->view('template/admin/header', $data);
        $this->load->view('template/admin/navbar', $data);
        $this->load->view('admin/pesanan', $data);
        $this->load->view('template/admin/footer', $data);
    }

    function get_target_pagination_base_url()
    {
    	$first_bit = $this->uri->segment(1);
    	//$second_bit = $this->uri->segment(2);
    	//$third_bit = $this->uri->segment(3);
    	//$target_base_url = base_url().$first_bit."/".$second_bit."/".$third_bit;
    	$target_base_url = base_url().$first_bit.'/index';
    	return $target_base_url;
    }

    function _generate_mysql_query($update_id = "", $use_limit)
    {
    	//NOTE: use_limit can be TRUE or FALSE

    	$mysql_query = "
    	SELECT invoice.*,pelanggan.username
    		FROM invoice
    			INNER JOIN pelanggan ON invoice.id_pelanggan = pelanggan.id_pelanggan
    		ORDER by invoice.status asc, invoice.id desc
    	";

    	if ($use_limit == TRUE) {
    		$limit = $this->get_limit();
    		$offset = $this->get_offset();
    		$mysql_query .= " limit ".$offset.", ".$limit;
    	}

    	return $mysql_query;
    }

    function get_limit()
    {
    	$limit = 10;
    	return $limit;
    }

    function get_offset()
    {
    	$offset = $this->uri->segment(3);
    	if (!is_numeric($offset)) {
    		$offset = 0;
    	}

    	return $offset;
    }

    function _custom_query($mysql_query)
    {
        $query = $this->invoice_model->_custom_query($mysql_query);
        return $query;
    }

    public function pesanan_detail_admin()
    {
        cek_login();

        $id = $this->uri->segment(3);

        $data = [
            'judul'     => 'Update Pengguna',
            'sub_judul' => 'Update Pengguna' . nama_toko,
        ];

        $where = array('invoice_id' => $id);
        $data['list_pesanan'] = $this->pesanan_model->get($where);

        $this->load->view('template/admin/header', $data);
        $this->load->view('template/admin/navbar', $data);
        $this->load->view('admin/pesanan_detail', $data);
        $this->load->view('template/admin/footer', $data);
    }

    public function confirm()
    {
        cek_login();
        $invoice_id = $this->input->post('id_pesanan');
        $where = array('id' => $invoice_id);
        $data = array('status' => 2);

        $this->invoice_model->update($data, $where);
        //echo $this->db->last_query(); die();

        redirect('pesanan');
    }

    public function checkout_update()
    {
        $total = $this->cart->total_items();
        $data  = [
            'total' => $this->cart->total_items(),
            'rowid' => $this->input->post('rowid'),
            'qty'   => $this->input->post('qty'),
        ];
        $this->cart->update($data);
        redirect('keranjang/tambah_keranjang');
    }

    public function simpan_pesanan()
    {
        $post = $this->input->post();
        //exit();
        $date = date("Y-m-d H:i:s");
        $date_invoice = date('Ymd');

        $price = 0;
        $jumlah = 0;
        foreach ($this->cart->contents() as $psn) {
            $price += $psn['subtotal'];
            $jumlah += $psn['qty'];
        }

        $invoice['id_pelanggan'] = $this->session->userdata('id_pelanggan');
        $invoice['metode_pengiriman'] = $post['metode_pengiriman'];
        $invoice['ongkir'] = $post['grand_total'] - $price;
        $invoice['jumlah'] = $jumlah;
        $invoice['totalbayar'] = $post['grand_total'];
        $invoice['status'] = '1';
        $invoice['created_date'] = $date;
        $invoice['created_by'] = $this->session->userdata('username');
        $invoice['last_update_date'] = $date;
        $invoice['last_update_by'] = $this->session->userdata('username');

        $this->invoice_model->insert($invoice);
        //echo $this->db->last_query(); die();
        $invoice_id = $this->db->insert_id();

        $where = array('id' => $invoice_id);
        $invoice_no = 'AHZ-'.$date_invoice.'-'.$this->session->userdata('id_pelanggan').'-'.$invoice_id;
        $invoice['invoice_no'] = $invoice_no;
        $this->invoice_model->update($invoice, $where);

        foreach ($this->cart->contents() as $psn) {
            //print_r('<pre>');
            //print_r($psn);
            //print_r($metode);
            // exit();
            $psn['metode'] = $post['metode_pengiriman'];
            $psn['invoice_id'] = $invoice_id;
            $pilih = $post['grand_total'];
            $this->pesanan_model->simpan_db($psn, $pilih, $date);
        }
        $this->cart->destroy();
        redirect('profil/info_pembayaran');
    }
 }
