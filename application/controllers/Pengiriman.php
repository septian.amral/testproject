<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengiriman extends CI_Controller
{

    public function index() {

    }

    public function provinsi()
    {
        echo cek_provinsi();
    }

    public function kota()
    {
        $provinsi = $this->input->post();
        $kota     = cek_kota($provinsi['provinsi']);
        print_r(json_encode($kota, true));
    }

    public function biaya()
    {
        $berat = $this->input->post('berat', true);
        $kota     = $this->input->post('kota', true);

        $olah = [
            'origin'      => '380',//380 Rembang
            'destination' => $kota,
            'weight'      => $berat*1000,
            'courier'     => 'jne',// saat ini support JNE
        ];
        $data['total'] = cek_harga($olah);
        print_r(json_encode($data, true));
    }

}

/* End of file Pengiriman.php */
/* Location: ./application/controllers/Pengiriman.php */
