<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(TRUE);
        $this->load->model(['produk_model', 'kategori_model']);
    }

    public function index($id_kategori = null)
    {
        if (!empty($id_kategori)) {
            $this->data_produk($id_kategori);
        } else {
            $this->data_produk($id_kategori);
        }
    }

    public function detail_produk($id_produk)
    {
        $data = [
            'judul'            => 'Ahza',
            'aktif'            => 'beranda',
            'sub_judul'        => 'Detail Produk ' . nama_toko,
            'data_produk'      => $this->produk_model->cek_produk($id_produk),
            'produk_lain'      => $this->produk_model->cek_produk(null, 'iya', 10),
            'data_kategori'    => $this->kategori_model->data_kategori(),
        ];
        $this->load->view('template/member/header', $data);
        $this->load->view('template/member/navbar', $data);
        $this->load->view('member/produk_detail', $data);
        $this->load->view('template/member/footer');
    }

    public function data_produk($id_kategori=null)
    {
        $data = [
            'aktif'            => 'beranda',
            'judul'            => 'Ahza',
            'sub_judul'        => 'Data Produk ' . nama_toko,
            'data_produk'      => $this->produk_model->data_produk($id_kategori),
            'data_kategori'    => $this->kategori_model->data_kategori(),
        ];


        $this->load->view('template/member/header', $data);
        $this->load->view('template/member/navbar', $data);
        $this->load->view('member/produk', $data);
        $this->load->view('template/member/footer');
    }
    public function data_produk_kategori()
    {
        $data = [
            'aktif'            => 'beranda',
            'judul'            => 'Ahza',
            'sub_judul'        => 'Data Produk ' . nama_toko,
            'data_produk'      => $this->produk_model->data_produk(),
            'data_kategori'    => $this->kategori_model->data_kategori(),
        ];


        $this->load->view('template/member/header', $data);
        $this->load->view('template/member/navbar', $data);
        $this->load->view('member/produk', $data);
        $this->load->view('template/member/footer');
    }
}
