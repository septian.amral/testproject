<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keranjang extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['produk_model', 'kategori_model']);
        $this->load->library('cart');
    }

    public function tambah_keranjang()
    {
        $images = $this->_upload_file();

        if ($this->input->post('id_produk') != null)
        {
            $produk = $this->produk_model->get_by_id($this->input->post('id_produk'));
            $data_pesanan = [
                'id'      => $this->input->post('id_produk'),
                'qty'     => $this->input->post('jumlah_produk'),
                'price'   => $this->input->post('harga'),
                'name'    => $this->input->post('namaproduk'),
                'berat'   => $produk['berat'],
                'gambar' => $produk['gambar'],
                'options' => [
                    'deskripsi' => $this->input->post('deskripsi'),
                    'gambar'    => isset($images[0]['file_name']) ? $images[0]['file_name'] : null,
                    'gambar2'   => isset($images[1]['file_name']) ? $images[1]['file_name'] : null,
                    'gambar3'   => isset($images[2]['file_name']) ? $images[2]['file_name'] : null,
                ],
            ];
            $this->cart->insert($data_pesanan);
        }

        $data = [
            'judul'         => 'Ahza',
            'aktif'         => 'beranda',
            'sub_judul'     => 'Detail Produk ' . nama_toko,
            'data_kategori' => $this->kategori_model->data_kategori(),
        ];

        $this->load->view('template/member/header', $data);
        $this->load->view('template/member/navbar', $data);
        $this->load->view('member/keranjang', $data);
        $this->load->view('template/member/footer');
    }

    public function hapus_keranjang($rowid)
    {
        if ("all" == $rowid) {
            $this->cart->destroy();
        } else {
            $this->cart->remove($rowid);
        }
        redirect('keranjang/tambah_keranjang', 'refresh');
    }

    private function _upload_file()
    {
        $dataInfo=array();
        //var_dump($_FILES['files']['name']); die();
        //echo ($_FILES['files']['name'][0] != ''); die();
        if(isset($_FILES['files']) && $_FILES['files']['name'][0] != ''){
            $dataInfo = [];
            $files    = $_FILES;
            
            $cpt      = count($_FILES['files']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['files']['name']     = $files['files']['name'][$i];
                $_FILES['files']['type']     = $files['files']['type'][$i];
                $_FILES['files']['tmp_name'] = $files['files']['tmp_name'][$i];
                $_FILES['files']['error']    = $files['files']['error'][$i];
                $_FILES['files']['size']     = $files['files']['size'][$i];

                $config['upload_path']   = './gambar/';
                $config['file_name']     = $this->session->userdata('id_pelanggan'). '-'.date('dmy').'-'.
                    $this->input->post('id_produk').'-'.$i;
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']      = '2048000';
                //$config['max_width']     = '1024';
                //$config['max_height']    = '768';

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('files')) {
                    $this->session->set_flashdata('gagal', $this->upload->display_errors());
                    redirect('produk/detail_produk/'.$this->input->post('id_produk'), 'refresh');
                }
                $dataInfo[] = $this->upload->data();
            }
        }

        return $dataInfo;
    }
}
